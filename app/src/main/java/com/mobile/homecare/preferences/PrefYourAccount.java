package com.mobile.homecare.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefYourAccount {
    private static final String DATA_DETAIL_YOUR_ACCOUNT = "detail_your_account",
            DATA_DETAIL_YOUR_ACCOUNT_FULL_NAME = "detail_your_account_fullName",
            DATA_DETAIL_YOUR_ACCOUNT_PHONE = "detail_your_account_phone",
            DATA_DETAIL_YOUR_ACCOUNT_PASSWORD = "detail_your_account_password",
            DATA_DETAIL_YOUR_ACCOUNT_GENDER = "detail_your_account_gender",
            DATA_DETAIL_YOUR_ACCOUNT_BIRTHDAY = "detail_your_account_birthday",
            DATA_DETAIL_YOUR_ACCOUNT_ADDRESS = "detail_your_account_address",
            DATA_DETAIL_YOUR_ACCOUNT_MAJOR_UUID = "detail_your_account_majorUuid",
            DATA_DETAIL_YOUR_ACCOUNT_MAJOR = "detail_your_account_major",
            DATA_DETAIL_YOUR_ACCOUNT_EXPERIENCE = "detail_your_account_experience",
            DATA_DETAIL_YOUR_ACCOUNT_JOB_TYPE = "detail_your_account_jobType",
            DATA_DETAIL_YOUR_ACCOUNT_POSITION_UUID = "detail_your_account_positionUuid",
            DATA_DETAIL_YOUR_ACCOUNT_POSITION = "detail_your_account_position";


    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setDataDetailYourAccountFullName(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_FULL_NAME, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountFullName(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_FULL_NAME, "");
    }

    public static void setDataDetailYourAccountPhone(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_PHONE, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountPhone(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_PHONE, "");
    }

    public static void setDataDetailYourAccountPassword(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_PASSWORD, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountPassword(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_PASSWORD, "");
    }

    public static void setDataDetailYourAccountGender(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_GENDER, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountGender(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_GENDER, "");
    }

    public static void setDataDetailYourAccountBirthday(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_BIRTHDAY, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountBirthday(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_BIRTHDAY, "");
    }

    public static void setDataDetailYourAccountAddress(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_ADDRESS, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountAddress(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_ADDRESS, "");
    }

    public static void setDataDetailYourAccountMajorUuid(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_MAJOR_UUID, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountMajorUuid(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_MAJOR_UUID, "");
    }

    public static void setDataDetailYourAccountMajor(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_MAJOR, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountMajor(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_MAJOR, "");
    }

    public static void setDataDetailYourAccountExperience(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_EXPERIENCE, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountExperience(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_EXPERIENCE, "");
    }

    public static void setDataDetailYourAccountJobType(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_JOB_TYPE, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountJobType(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_JOB_TYPE, "");
    }

    public static void setDataDetailYourAccountPositionUuid(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_POSITION_UUID, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountPositionUuid(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_POSITION_UUID, "");
    }

    public static void setDataDetailYourAccountPosition(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_YOUR_ACCOUNT_POSITION, data);
        editor.apply();
    }

    public static String getDataDetailYourAccountPosition(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_YOUR_ACCOUNT_POSITION, "");
    }

    public static void setDataDetailYourAccount(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(DATA_DETAIL_YOUR_ACCOUNT, status);
        editor.apply();
    }

    public static boolean getDataDetailYourAccount(Context context) {
        return getSharedPreferences(context).getBoolean(DATA_DETAIL_YOUR_ACCOUNT, false);
    }

    public static void clearDataDetailYourAccount(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_FULL_NAME);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_PHONE);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_PASSWORD);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_GENDER);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_ADDRESS);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_MAJOR);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_MAJOR_UUID);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_EXPERIENCE);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_JOB_TYPE);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_POSITION);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT_POSITION_UUID);
        editor.remove(DATA_DETAIL_YOUR_ACCOUNT);
        editor.apply();
    }
}
