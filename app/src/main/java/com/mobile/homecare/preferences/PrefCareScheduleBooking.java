package com.mobile.homecare.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.preference.PreferenceManager;

public class PrefCareScheduleBooking {
    private static final String CARE_SCHEDULE_BOOKING = "care_schedule_booking",
            CARE_SCHEDULE_BOOKING_FULL_NAME = "care_schedule_booking_fullName",
            CARE_SCHEDULE_BOOKING_PHONE = "care_schedule_booking_phone",
            CARE_SCHEDULE_BOOKING_ADDRESS = "care_schedule_booking_address",
            CARE_SCHEDULE_BOOKING_CONTENT = "care_schedule_booking_content",
            CARE_SCHEDULE_BOOKING_FROM_DATE = "care_schedule_booking_from_date",
            CARE_SCHEDULE_BOOKING_TO_DATE = "care_schedule_booking_to_date",
            CARE_SCHEDULE_BOOKING_CARE_TIME = "care_schedule_booking_care_time",
            CARE_SCHEDULE_BOOKING_FOLLOWER = "care_schedule_booking_follower";

    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setDataCareScheduleBookingFullName(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CARE_SCHEDULE_BOOKING_FULL_NAME, data);
        editor.apply();
    }

    public static String getDataCareScheduleBookingFullName(Context context) {
        return getSharedPreferences(context).getString(CARE_SCHEDULE_BOOKING_FULL_NAME, "");
    }

    public static void setDataCareScheduleBookingPhone(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CARE_SCHEDULE_BOOKING_PHONE, data);
        editor.apply();
    }

    public static String getDataCareScheduleBookingPhone(Context context) {
        return getSharedPreferences(context).getString(CARE_SCHEDULE_BOOKING_PHONE, "");
    }

    public static void setDataCareScheduleBookingAddress(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CARE_SCHEDULE_BOOKING_ADDRESS, data);
        editor.apply();
    }

    public static String getDataCareScheduleBookingAddress(Context context) {
        return getSharedPreferences(context).getString(CARE_SCHEDULE_BOOKING_ADDRESS, "");
    }

    public static void setDataCareScheduleBookingContent(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CARE_SCHEDULE_BOOKING_CONTENT, data);
        editor.apply();
    }

    public static String getDataCareScheduleBookingContent(Context context) {
        return getSharedPreferences(context).getString(CARE_SCHEDULE_BOOKING_CONTENT, "");
    }

    public static void setDataCareScheduleBookingFromDate(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CARE_SCHEDULE_BOOKING_FROM_DATE, data);
        editor.apply();
    }

    public static String getDataCareScheduleBookingFromDate(Context context) {
        return getSharedPreferences(context).getString(CARE_SCHEDULE_BOOKING_FROM_DATE, "");
    }

    public static void setDataCareScheduleBookingToDate(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CARE_SCHEDULE_BOOKING_TO_DATE, data);
        editor.apply();
    }

    public static String getDataCareScheduleBookingToDate(Context context) {
        return getSharedPreferences(context).getString(CARE_SCHEDULE_BOOKING_TO_DATE, "");
    }

    public static void setDataCareScheduleBookingCareTime(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CARE_SCHEDULE_BOOKING_CARE_TIME, data);
        editor.apply();
    }

    public static String getDataCareScheduleBookingCareTime(Context context) {
        return getSharedPreferences(context).getString(CARE_SCHEDULE_BOOKING_CARE_TIME, "");
    }

    public static void setDataCareScheduleBookingFollower(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(CARE_SCHEDULE_BOOKING_FOLLOWER, data);
        editor.apply();
    }

    public static String getDataCareScheduleBookingFollower(Context context) {
        return getSharedPreferences(context).getString(CARE_SCHEDULE_BOOKING_FOLLOWER, "");
    }

    public static void setDataCareScheduleBooking(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(CARE_SCHEDULE_BOOKING, status);
        editor.apply();
    }

    public static boolean getDataCareScheduleBooking(Context context) {
        return getSharedPreferences(context).getBoolean(CARE_SCHEDULE_BOOKING, false);
    }

    public static void clearDataCareScheduleBooking(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.remove(CARE_SCHEDULE_BOOKING_FULL_NAME);
        editor.remove(CARE_SCHEDULE_BOOKING_PHONE);
        editor.remove(CARE_SCHEDULE_BOOKING_ADDRESS);
        editor.remove(CARE_SCHEDULE_BOOKING_CONTENT);
        editor.remove(CARE_SCHEDULE_BOOKING_FROM_DATE);
        editor.remove(CARE_SCHEDULE_BOOKING_TO_DATE);
        editor.remove(CARE_SCHEDULE_BOOKING_CARE_TIME);
        editor.remove(CARE_SCHEDULE_BOOKING_FOLLOWER);
        editor.remove(CARE_SCHEDULE_BOOKING);
        editor.apply();
    }

}
