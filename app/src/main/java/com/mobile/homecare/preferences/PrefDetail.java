package com.mobile.homecare.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefDetail {
    private static final String DATA_DETAIL = "detail",
            DATA_DETAIL_FULL_NAME = "detail_fullName",
            DATA_DETAIL_PHONE = "detail_phone",
            DATA_DETAIL_PASSWORD = "detail_password",
            DATA_DETAIL_GENDER = "detail_gender",
            DATA_DETAIL_BIRTHDAY = "detail_birthday",
            DATA_DETAIL_ADDRESS = "detail_address",
            DATA_DETAIL_MAJOR_UUID = "detail_majorUuid",
            DATA_DETAIL_MAJOR = "detail_major",
            DATA_DETAIL_EXPERIENCE = "detail_experience",
            DATA_DETAIL_JOB_TYPE = "detail_jobType",
            DATA_DETAIL_POSITION_UUID = "detail_positionUuid",
            DATA_DETAIL_POSITION = "detail_position";


    private static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setDataDetailFullName(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_FULL_NAME, data);
        editor.apply();
    }

    public static String getDataDetailFullName(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_FULL_NAME, "");
    }

    public static void setDataDetailPhone(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_PHONE, data);
        editor.apply();
    }

    public static String getDataDetailPhone(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_PHONE, "");
    }

    public static void setDataDetailPassword(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_PASSWORD, data);
        editor.apply();
    }

    public static String getDataDetailPassword(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_PASSWORD, "");
    }

    public static void setDataDetailGender(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_GENDER, data);
        editor.apply();
    }

    public static String getDataDetailGender(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_GENDER, "");
    }

    public static void setDataDetailBirthday(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_BIRTHDAY, data);
        editor.apply();
    }

    public static String getDataDetailBirthday(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_BIRTHDAY, "");
    }

    public static void setDataDetailAddress(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_ADDRESS, data);
        editor.apply();
    }

    public static String getDataDetailAddress(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_ADDRESS, "");
    }

    public static void setDataDetailMajorUuid(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_MAJOR_UUID, data);
        editor.apply();
    }

    public static String getDataDetailMajorUuid(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_MAJOR_UUID, "");
    }

    public static void setDataDetailMajor(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_MAJOR, data);
        editor.apply();
    }

    public static String getDataDetailMajor(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_MAJOR, "");
    }

    public static void setDataDetailExperience(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_EXPERIENCE, data);
        editor.apply();
    }

    public static String getDataDetailExperience(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_EXPERIENCE, "");
    }

    public static void setDataDetailJobType(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_JOB_TYPE, data);
        editor.apply();
    }

    public static String getDataDetailJobType(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_JOB_TYPE, "");
    }

    public static void setDataDetailPositionUuid(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_POSITION_UUID, data);
        editor.apply();
    }

    public static String getDataDetailPositionUuid(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_POSITION_UUID, "");
    }

    public static void setDataDetailPosition(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(DATA_DETAIL_POSITION, data);
        editor.apply();
    }

    public static String getDataDetailPosition(Context context) {
        return getSharedPreferences(context).getString(DATA_DETAIL_POSITION, "");
    }

    public static void setDataDetail(Context context, boolean status) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(DATA_DETAIL, status);
        editor.apply();
    }

    public static boolean getDataDetail(Context context) {
        return getSharedPreferences(context).getBoolean(DATA_DETAIL, false);
    }

    public static void clearDataDetail(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.remove(DATA_DETAIL_FULL_NAME);
        editor.remove(DATA_DETAIL_PHONE);
        editor.remove(DATA_DETAIL_PASSWORD);
        editor.remove(DATA_DETAIL_GENDER);
        editor.remove(DATA_DETAIL_ADDRESS);
        editor.remove(DATA_DETAIL_MAJOR);
        editor.remove(DATA_DETAIL_MAJOR_UUID);
        editor.remove(DATA_DETAIL_EXPERIENCE);
        editor.remove(DATA_DETAIL_JOB_TYPE);
        editor.remove(DATA_DETAIL_POSITION);
        editor.remove(DATA_DETAIL_POSITION_UUID);
        editor.remove(DATA_DETAIL);
        editor.apply();
    }
}
