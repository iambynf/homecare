package com.mobile.homecare.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.homecare.R;
import com.mobile.homecare.adapter.ListCareScheduleBookingAdapter;
import com.mobile.homecare.models.CareScheduleBooking;


public class ListCareScheduleBookingFragment extends Fragment {
    RecyclerView listCareScheduleBooking;
    ListCareScheduleBookingAdapter scheduleBookingAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_list_care_schedule_booking, container, false);
        listCareScheduleBooking = view.findViewById(R.id.fm_list_care_schedule_booking_recyclerview);
        listCareScheduleBooking.setLayoutManager(new LinearLayoutManager(view.getContext()));

        FirebaseRecyclerOptions<CareScheduleBooking> options = new FirebaseRecyclerOptions.Builder<CareScheduleBooking>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("scheduleBookings").orderByChild("content").equalTo("Chăm sóc"), CareScheduleBooking.class)
                .build();

        scheduleBookingAdapter = new ListCareScheduleBookingAdapter(options);
        listCareScheduleBooking.setAdapter(scheduleBookingAdapter);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        scheduleBookingAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        scheduleBookingAdapter.stopListening();
    }
}