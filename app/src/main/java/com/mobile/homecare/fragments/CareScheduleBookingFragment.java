package com.mobile.homecare.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.utilities.Tree;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.booking.ScheduleBookingManagementActivity;
import com.mobile.homecare.models.CareScheduleBooking;
import com.mobile.homecare.models.Customer;
import com.mobile.homecare.preferences.PrefDetail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TreeMap;


public class CareScheduleBookingFragment extends Fragment implements View.OnClickListener {

    TextInputEditText content, careTime, fromDate, toDate, fullName, phone, address;
    Button careScheduleBooking, careScheduleBookingHavePhone;
    DatePickerDialog.OnDateSetListener onDateFromDate, onDateToDate;
    AutoCompleteTextView autoFollower;
    TreeMap<String, String> followerTree= new TreeMap<>();
    TreeMap<String, String> majorTree= new TreeMap<>();
    TreeMap<String, String> positionTree = new TreeMap<>();
    int hour, minute;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_care_schedule_booking, container, false);
        content = view.findViewById(R.id.etCareContent);
        careTime = view.findViewById(R.id.etCareTime);
        fromDate = view.findViewById(R.id.etFromDate);
        toDate = view.findViewById(R.id.etToDate);
        fullName = view.findViewById(R.id.care_name_edit_text);
        phone = view.findViewById(R.id.care_phone_edit_text);
        address = view.findViewById(R.id.care_address_edit_text);
        autoFollower = view.findViewById(R.id.autoFollower);
        careScheduleBooking = view.findViewById(R.id.care_schedule_booking_button);
        careScheduleBookingHavePhone = view.findViewById(R.id.care_schedule_booking_have_phone_button);
        careTime.setOnClickListener(this);
        fromDate.setOnClickListener(this);
        toDate.setOnClickListener(this);
        String phoneCustomer = PrefDetail.getDataDetailPhone(getContext());
        if (phoneCustomer.equals("")) {
            careScheduleBookingHavePhone.setVisibility(View.GONE);
            careScheduleBooking.setOnClickListener(this);
        } else {
            phone.setText(phoneCustomer);
            getPatientDetails(phoneCustomer);
            careScheduleBookingHavePhone.setOnClickListener(this);
            careScheduleBooking.setVisibility(View.GONE);
        }


        ArrayList<String> arrayFollower = new ArrayList<>();
        ArrayAdapter<String> adapterFollower = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_dropdown_item_1line, arrayFollower);
        getRoles();
        getPosition();
        getFollower(arrayFollower);
        autoFollower.setAdapter(adapterFollower);
        onDateFromDate = (datePicker, year, month, dayOfMonth) -> {
            month = month + 1;
            String date = dayOfMonth + "/" + month + "/" +year;
            fromDate.setText(date);
        };
        onDateToDate = (datePicker, year, month, dayOfMonth) -> {
            month = month + 1;
            String date = dayOfMonth + "/" + month + "/" +year;
            toDate.setText(date);
        };
        return view;
    }

    @Override
    public void onClick(View view) {
        String content, fromDate, toDate, time, careTakerPhone, phone, fullName, address, content_phone, content_careTakerPhone;
        SimpleDateFormat dateFormat;
        Date date;
        CareScheduleBooking scheduleBooking;
        DatabaseReference refCreateScheduleBookings;
        switch (view.getId()) {
            case R.id.etCareTime:
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        view.getContext(),
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minuteOfHour) {
                                hour = hourOfDay;
                                minute = minuteOfHour;
                                Calendar calendar = Calendar.getInstance();
                                //Set hour and minute
                                calendar.set(0, 0,0, hour, minute);
                                //Set selected time on text view
                                careTime.setText(DateFormat.format("hh:mm aa", calendar));
                            }
                        }, 24, 0, true
                );
                //Displayed previous selected time
                timePickerDialog.updateTime(hour, minute);
                //Show dialog
                timePickerDialog.show();
                break;
            case R.id.etFromDate:
                final Calendar calendarFromDate = Calendar.getInstance();
                int yearFromDate = calendarFromDate.get(Calendar.YEAR);
                int monthFromDate = calendarFromDate.get(Calendar.MONTH);
                int dayFromDate = calendarFromDate.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialogFromDate = new DatePickerDialog(view.getContext(), onDateFromDate, yearFromDate, monthFromDate, dayFromDate);
                datePickerDialogFromDate.show();
                break;
            case R.id.etToDate:
                final Calendar calendarToDate = Calendar.getInstance();
                int yearToDate = calendarToDate.get(Calendar.YEAR);
                int monthToDate = calendarToDate.get(Calendar.MONTH);
                int dayToDate = calendarToDate.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialogToDate = new DatePickerDialog(view.getContext(), onDateToDate, yearToDate, monthToDate, dayToDate);
                datePickerDialogToDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialogToDate.show();
                break;
            case R.id.care_schedule_booking_button:
                content = Objects.requireNonNull(this.content.getText()).toString();
                fromDate = Objects.requireNonNull(this.fromDate.getText()).toString();
                toDate = Objects.requireNonNull(this.toDate.getText()).toString();
                time = Objects.requireNonNull(this.careTime.getText()).toString();
                careTakerPhone = followerTree.get(autoFollower.getText().toString());
                fullName = this.fullName.getText().toString();
                phone = this.phone.getText().toString();
                address = this.address.getText().toString();
                dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                content_phone = content + "_" + phone;
                content_careTakerPhone = content + "_" + careTakerPhone;
                date = new Date(System.currentTimeMillis());
                scheduleBooking = new CareScheduleBooking(java.util.UUID.randomUUID().toString(), fullName, phone, address, content, fromDate, toDate, time, careTakerPhone, content_phone, content_careTakerPhone, dateFormat.format(date));
                refCreateScheduleBookings = FirebaseDatabase.getInstance().getReference("scheduleBookings");
                refCreateScheduleBookings.child(scheduleBooking.getUuid()).setValue(scheduleBooking)
                        .addOnSuccessListener(unused -> Toast.makeText(getContext(), "Đặt lịch thành công", Toast.LENGTH_SHORT).show())
                        .addOnFailureListener(e -> Toast.makeText(getContext(), "Đặt lịch thất bại, vui lòng kiểm tra lại", Toast.LENGTH_SHORT).show());
                Customer customer = new Customer(fullName, address, phone, followerTree.get(careTakerPhone), positionTree.get("Patient"));
                DatabaseReference refCreateCustomer = FirebaseDatabase.getInstance().getReference("customers");
                refCreateCustomer.child(customer.getPhone()).setValue(customer);
                startActivity(new Intent(getContext(), ScheduleBookingManagementActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.care_schedule_booking_have_phone_button:
                content = Objects.requireNonNull(this.content.getText()).toString();
                fromDate = Objects.requireNonNull(this.fromDate.getText()).toString();
                toDate = Objects.requireNonNull(this.toDate.getText()).toString();
                time = Objects.requireNonNull(this.careTime.getText()).toString();
                careTakerPhone = followerTree.get(autoFollower.getText().toString());
                fullName = this.fullName.getText().toString();
                phone = this.phone.getText().toString();
                address = this.address.getText().toString();
                content_phone = content + "_" + phone;
                content_careTakerPhone = content + "_" + careTakerPhone;
                dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                date = new Date(System.currentTimeMillis());
                scheduleBooking = new CareScheduleBooking(java.util.UUID.randomUUID().toString(), fullName, phone, address, content, fromDate, toDate, time, careTakerPhone, content_phone, content_careTakerPhone, dateFormat.format(date));
                refCreateScheduleBookings = FirebaseDatabase.getInstance().getReference("scheduleBookings");
                refCreateScheduleBookings.child(scheduleBooking.getUuid()).setValue(scheduleBooking).addOnSuccessListener(unused -> {
                    PrefDetail.clearDataDetail(getContext());
                    Toast.makeText(getContext(), "Đặt lịch thành công", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getContext(), ScheduleBookingManagementActivity.class));
                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                }).addOnFailureListener(e -> Toast.makeText(getContext(), "Đặt lịch thất bại, vui lòng kiểm tra lại", Toast.LENGTH_SHORT).show());

                break;
        }
    }

    private void getRoles() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("roles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    majorTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void getFollower(ArrayList<String> arrayFollower) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("doctors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (Objects.equals(dataSnapshot.child("roleUuid").getValue(String.class), majorTree.get("Nursing"))) {
                        followerTree.put(dataSnapshot.child("fullName").getValue(String.class), dataSnapshot.child("phone").getValue(String.class));
                        arrayFollower.add(dataSnapshot.child("fullName").getValue(String.class));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void getPosition() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("roles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (Objects.equals(dataSnapshot.child("name").getValue(String.class), "Patient")) {
                        positionTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getPatientDetails(String phone) {
        DatabaseReference refPatientDetail = FirebaseDatabase.getInstance().getReference();
        refPatientDetail.child("customers").child(phone).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                fullName.setText(snapshot.child("fullName").getValue(String.class));
                address.setText(snapshot.child("address").getValue(String.class));
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}