package com.mobile.homecare.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.booking.ScheduleBookingManagementActivity;
import com.mobile.homecare.models.Customer;
import com.mobile.homecare.models.MedicalExaminationScheduleBooking;
import com.mobile.homecare.preferences.PrefDetail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TreeMap;

public class MedicalExaminationScheduleBookingFragment extends Fragment implements View.OnClickListener {

    TextInputEditText medicalExamination, medicalExaminationDate, medicalExaminationTime, describeSymptoms, fullName, phone, address;
    AutoCompleteTextView autoMajor, autoDoctor;
    Button medicalExaminationScheduleBooking, medicalExaminationScheduleBookingHavePhone;
    DatePickerDialog.OnDateSetListener onDateMedicalExaminationDate;
    TreeMap<String, String> majorTree = new TreeMap<>();
    TreeMap<String, String> doctorTree = new TreeMap<>();
    TreeMap<String, String> positionTree = new TreeMap<>();
    ArrayList<String> arrayMajor;
    ArrayList<String> arrayDoctor;
    ArrayAdapter<String> adapterMajor;
    ArrayAdapter<String> adapterDoctor;
    int hour, minute;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medical_examination_schedule_booking, container, false);
        medicalExamination = view.findViewById(R.id.etMedicalExamination);
        medicalExaminationDate = view.findViewById(R.id.etMedicalExaminationDate);
        medicalExaminationTime = view.findViewById(R.id.etMedicalExaminationTime);
        describeSymptoms = view.findViewById(R.id.etDescribeSymptoms);
        fullName = view.findViewById(R.id.medical_exam_name_edit_text);
        phone = view.findViewById(R.id.medical_exam_phone_edit_text);
        address = view.findViewById(R.id.medical_exam_address_edit_text);
        autoMajor = view.findViewById(R.id.autoMajor);
        autoDoctor = view.findViewById(R.id.autoDoctor);
        String phoneCustomer = PrefDetail.getDataDetailPhone(getContext());
        medicalExaminationScheduleBooking = view.findViewById(R.id.medical_examination_schedule_booking_button);
        medicalExaminationScheduleBookingHavePhone = view.findViewById(R.id.medical_examination_schedule_booking_have_phone_button);
        if (phoneCustomer.equals("")) {
            medicalExaminationScheduleBookingHavePhone.setVisibility(View.GONE);
        } else {
            phone.setText(phoneCustomer);
            getPatientDetails(phoneCustomer);
            medicalExaminationScheduleBooking.setVisibility(View.GONE);
        }
        arrayMajor = new ArrayList<>();
        arrayDoctor = new ArrayList<>();
        adapterMajor = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, arrayMajor);
        adapterDoctor = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, arrayDoctor);
        getMajor(arrayMajor);
        getPosition();
        autoMajor.setAdapter(adapterMajor);
        medicalExaminationDate.setOnClickListener(this);
        medicalExaminationTime.setOnClickListener(this);
        medicalExaminationScheduleBooking.setOnClickListener(this);
        medicalExaminationScheduleBookingHavePhone.setOnClickListener(this);
        autoMajor.setOnItemClickListener((adapterView, view1, i, l) -> {
            getDoctor(arrayDoctor);
            autoDoctor.setAdapter(adapterDoctor);
        });

        onDateMedicalExaminationDate = (datePicker, year, month, dayOfMonth) -> {
            month = month + 1;
            String date = dayOfMonth + "/" + month + "/" +year;
            medicalExaminationDate.setText(date);
        };
        return view;
    }

    private void getMajor(ArrayList<String> arrayMajor) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("majors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    majorTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                    arrayMajor.add(dataSnapshot.child("name").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void getDoctor(ArrayList<String> arrayDoctor) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("doctors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                adapterDoctor.clear();
                autoDoctor.setText("", false);
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (Objects.equals(dataSnapshot.child("majorUuid").getValue(String.class), majorTree.get(autoMajor.getText().toString()))) {
                        doctorTree.put(dataSnapshot.child("fullName").getValue(String.class), dataSnapshot.child("phone").getValue(String.class));
                        arrayDoctor.add(dataSnapshot.child("fullName").getValue(String.class));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        String medicalExamination, medicalExaminationDate, time, describeSymptoms, majorUuid, doctorPhone, fullName, phone, address, content_phone, content_doctorPhone;
        MedicalExaminationScheduleBooking medicalExaminationScheduleBooking;
        DatabaseReference refCreateScheduleBookings;
        SimpleDateFormat dateFormat;
        Date date;

        switch(view.getId()) {
            case R.id.etMedicalExaminationDate:
                final Calendar calendarFromDate = Calendar.getInstance();
                int yearFromDate = calendarFromDate.get(Calendar.YEAR);
                int monthFromDate = calendarFromDate.get(Calendar.MONTH);
                int dayFromDate = calendarFromDate.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialogFromDate = new DatePickerDialog(view.getContext(), onDateMedicalExaminationDate, yearFromDate, monthFromDate, dayFromDate);
                datePickerDialogFromDate.show();
                break;
            case R.id.etMedicalExaminationTime:
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        view.getContext(),
                        (timePicker, hourOfDay, minuteOfHour) -> {
                            hour = hourOfDay;
                            minute = minuteOfHour;
                            Calendar calendar = Calendar.getInstance();
                            //Set hour and minute
                            calendar.set(0, 0,0, hour, minute);
                            //Set selected time on text view
                            medicalExaminationTime.setText(DateFormat.format("hh:mm aa", calendar));
                        }, 24, 0, true
                );
                //Displayed previous selected time
                timePickerDialog.updateTime(hour, minute);
                //Show dialog
                timePickerDialog.show();
                break;
            case R.id.medical_examination_schedule_booking_button:

                medicalExamination = this.medicalExamination.getText().toString();
                medicalExaminationDate = this.medicalExaminationDate.getText().toString();
                time = this.medicalExaminationTime.getText().toString();
                describeSymptoms = this.describeSymptoms.getText().toString();
                fullName = this.fullName.getText().toString();
                phone = this.phone.getText().toString();
                address = this.address.getText().toString();
                majorUuid = this.autoMajor.getText().toString();
                doctorPhone = doctorTree.get(this.autoDoctor.getText().toString());
                dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                content_phone = medicalExamination + "_" + phone;
                content_doctorPhone = medicalExamination + "_" + doctorPhone;
                date = new Date(System.currentTimeMillis());
                medicalExaminationScheduleBooking =
                        new MedicalExaminationScheduleBooking(
                                java.util.UUID.randomUUID().toString(), fullName, phone, address,
                                medicalExamination,
                                medicalExaminationDate,
                                time,
                                describeSymptoms,
                                majorTree.get(majorUuid),
                                doctorPhone,
                                content_phone,
                                content_doctorPhone,
                                dateFormat.format(date));
                refCreateScheduleBookings = FirebaseDatabase.getInstance().getReference("scheduleBookings");
                refCreateScheduleBookings.child(medicalExaminationScheduleBooking.getUuid()).setValue(medicalExaminationScheduleBooking)
                        .addOnSuccessListener(unused -> { Toast.makeText(getContext(), "Đặt lịch thành công", Toast.LENGTH_SHORT).show();
                        }).addOnFailureListener(e -> { Toast.makeText(getContext(), "Đặt lịch thất bại", Toast.LENGTH_SHORT).show();
                });
                Toast.makeText(getContext(), "Đặt lịch thành công", Toast.LENGTH_SHORT).show();

                Customer customer = new Customer(fullName, address, phone, doctorTree.get(doctorPhone), positionTree.get("Patient"));
                DatabaseReference refCreateCustomer = FirebaseDatabase.getInstance().getReference("customers");
                refCreateCustomer.child(customer.getPhone()).setValue(customer);

                startActivity(new Intent(getContext(), ScheduleBookingManagementActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.medical_examination_schedule_booking_have_phone_button:
                medicalExamination = this.medicalExamination.getText().toString();
                medicalExaminationDate = this.medicalExaminationDate.getText().toString();
                time = this.medicalExaminationTime.getText().toString();
                describeSymptoms = this.describeSymptoms.getText().toString();
                fullName = this.fullName.getText().toString();
                phone = this.phone.getText().toString();
                address = this.address.getText().toString();
                majorUuid = this.autoMajor.getText().toString();
                doctorPhone = doctorTree.get(this.autoDoctor.getText().toString());
                dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                content_phone = medicalExamination + "_" + phone;
                content_doctorPhone = medicalExamination + "_" + doctorPhone;
                date = new Date(System.currentTimeMillis());
                medicalExaminationScheduleBooking =
                        new MedicalExaminationScheduleBooking(
                                java.util.UUID.randomUUID().toString(), fullName, phone, address,
                                medicalExamination,
                                medicalExaminationDate,
                                time,
                                describeSymptoms,
                                majorTree.get(majorUuid),
                                doctorPhone,
                                content_phone,
                                content_doctorPhone,
                                dateFormat.format(date));
                refCreateScheduleBookings = FirebaseDatabase.getInstance().getReference("scheduleBookings");
                refCreateScheduleBookings.child(medicalExaminationScheduleBooking.getUuid()).setValue(medicalExaminationScheduleBooking)
                        .addOnSuccessListener(unused -> {
                            PrefDetail.clearDataDetail(getContext());
                            Toast.makeText(getContext(), "Đặt lịch thành công", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getContext(), ScheduleBookingManagementActivity.class));
                            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                    }).addOnFailureListener(e -> {
                        Toast.makeText(getContext(), "Đặt lịch thất bại", Toast.LENGTH_SHORT).show();
                });

                break;
        }
    }

    private void getPosition() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("roles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (Objects.equals(dataSnapshot.child("name").getValue(String.class), "Patient")) {
                        positionTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getPatientDetails(String phone) {
        DatabaseReference refPatientDetail = FirebaseDatabase.getInstance().getReference();
        refPatientDetail.child("customers").child(phone).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                fullName.setText(snapshot.child("fullName").getValue(String.class));
                address.setText(snapshot.child("address").getValue(String.class));
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}