package com.mobile.homecare.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.homecare.R;
import com.mobile.homecare.adapter.ListMedicalExaminationScheduleBookingAdapter;
import com.mobile.homecare.models.MedicalExaminationScheduleBooking;
import com.mobile.homecare.preferences.PrefDetail;

public class ListMedicalExaminationScheduleBookingPatientFragment extends Fragment {
    RecyclerView listMedicalExaminationScheduleBooking;
    ListMedicalExaminationScheduleBookingAdapter scheduleBookingAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_medical_examination_schedule_booking, container, false);
        listMedicalExaminationScheduleBooking = view.findViewById(R.id.fm_list_medical_examination_schedule_booking_recyclerview);
        listMedicalExaminationScheduleBooking.setLayoutManager(new LinearLayoutManager(view.getContext()));
        String patientPhone = PrefDetail.getDataDetailPhone(view.getContext());
        FirebaseRecyclerOptions<MedicalExaminationScheduleBooking> options = new FirebaseRecyclerOptions.Builder<MedicalExaminationScheduleBooking>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("scheduleBookings").orderByChild("content_phone").equalTo("Khám bệnh_"+patientPhone), MedicalExaminationScheduleBooking.class)
                .build();

        scheduleBookingAdapter = new ListMedicalExaminationScheduleBookingAdapter(options);
        listMedicalExaminationScheduleBooking.setAdapter(scheduleBookingAdapter);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        scheduleBookingAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        scheduleBookingAdapter.stopListening();
    }
}