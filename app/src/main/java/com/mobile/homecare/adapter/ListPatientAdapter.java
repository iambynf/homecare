package com.mobile.homecare.adapter;

import android.app.AlertDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.booking.CareAndMedicalExaminationScheduleBookingActivity;
import com.mobile.homecare.activity.patient.ListCareAndMedicalExaminationScheduleBookingPatientActivity;
import com.mobile.homecare.activity.patient.UpdatePatientActivity;
import com.mobile.homecare.models.Customer;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.Objects;

public class ListPatientAdapter extends FirebaseRecyclerAdapter<Customer, ListPatientAdapter.myViewHolder> {

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ListPatientAdapter(@NonNull FirebaseRecyclerOptions<Customer> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull myViewHolder holder, int position, @NonNull Customer model) {
        holder.fullName.setText(model.getFullName());
        holder.phone.setText(model.getPhone());
        DatabaseReference refFollower = FirebaseDatabase.getInstance().getReference();
        refFollower.child("doctors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.child(model.getFollowerPhone()).exists()) {
                    holder.follower.setText(snapshot.child(model.getFollowerPhone()).child("fullName").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        holder.update.setOnClickListener(view -> {
            PrefDetail.setDataDetailPhone(holder.phone.getContext(),model.getPhone());
            holder.phone.getContext().startActivity(new Intent(holder.phone.getContext(), UpdatePatientActivity.class));
        });

        holder.delete.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(holder.phone.getContext());
            builder.setTitle("Bạn chắc chắn chưa?");
            builder.setMessage("Dữ liệu không thể khôi phục sau khi xóa.");
            builder.setPositiveButton("Xóa", (dialogInterface, i) -> FirebaseDatabase.getInstance().getReference()
                    .child("customers")
                    .child(Objects.requireNonNull(getRef(holder.getBindingAdapterPosition()).getKey()))
                    .removeValue());
            builder.setNegativeButton("Quay lại", (dialogInterface, i) -> {
            });
            builder.show();
        });

        holder.listScheduleBooking.setOnClickListener(view -> {
            PrefDetail.setDataDetailPhone(holder.phone.getContext(), model.getPhone());
            holder.phone.getContext().startActivity(new Intent(holder.phone.getContext(), ListCareAndMedicalExaminationScheduleBookingPatientActivity.class));

        });

        holder.bookingCareAndMedicalExamination.setOnClickListener(view -> {
            PrefDetail.setDataDetailPhone(holder.phone.getContext(), model.getPhone());
            holder.phone.getContext().startActivity(new Intent(holder.phone.getContext(), CareAndMedicalExaminationScheduleBookingActivity.class));
        });

    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_patient, parent, false);
        return new ListPatientAdapter.myViewHolder(view);
    }

    class myViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView fullName, phone, follower;
        ImageButton update, delete;
        Button listScheduleBooking, bookingCareAndMedicalExamination;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);

            update = itemView.findViewById(R.id.list_schedule_booking_update_button);
            delete = itemView.findViewById(R.id.list_schedule_booking_delete_button);
            img = itemView.findViewById(R.id.imgAvatar);
            fullName = itemView.findViewById(R.id.list_schedule_booking_full_name_textview);
            phone = itemView.findViewById(R.id.list_schedule_booking_phone_text_view);
            delete = itemView.findViewById(R.id.list_schedule_booking_delete_button);
            follower = itemView.findViewById(R.id.tvFollower);
            listScheduleBooking = itemView.findViewById(R.id.list_schedule_booking_button);
            bookingCareAndMedicalExamination = itemView.findViewById(R.id.schedule_booking_button);
        }
    }
}
