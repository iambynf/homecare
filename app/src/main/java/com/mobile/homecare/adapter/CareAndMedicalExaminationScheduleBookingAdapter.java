package com.mobile.homecare.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.mobile.homecare.fragments.CareScheduleBookingFragment;
import com.mobile.homecare.fragments.MedicalExaminationScheduleBookingFragment;

import java.util.ArrayList;

import io.reactivex.rxjava3.internal.operators.single.SingleFlatMapIterableFlowable;

public class CareAndMedicalExaminationScheduleBookingAdapter extends FragmentStateAdapter {


    public CareAndMedicalExaminationScheduleBookingAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0) {
            return new MedicalExaminationScheduleBookingFragment();
        } else {
            return new CareScheduleBookingFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

}
