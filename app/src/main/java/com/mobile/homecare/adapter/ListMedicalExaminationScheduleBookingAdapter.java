package com.mobile.homecare.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.models.CareScheduleBooking;
import com.mobile.homecare.models.MedicalExaminationScheduleBooking;

import java.util.Objects;

public class ListMedicalExaminationScheduleBookingAdapter extends FirebaseRecyclerAdapter<MedicalExaminationScheduleBooking, ListMedicalExaminationScheduleBookingAdapter.myViewHolder> {

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ListMedicalExaminationScheduleBookingAdapter(@NonNull FirebaseRecyclerOptions<MedicalExaminationScheduleBooking> options) {
        super(options);
    }


    @Override
    protected void onBindViewHolder(@NonNull myViewHolder holder, int position, @NonNull MedicalExaminationScheduleBooking model) {
        holder.fullName.setText(model.getFullName());
        holder.phone.setText(model.getPhone());
        DatabaseReference refFollower = FirebaseDatabase.getInstance().getReference();
        refFollower.child("doctors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.child(model.getDoctorPhone()).exists()) {
                    holder.follower.setText(snapshot.child(model.getDoctorPhone()).child("fullName").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.delete.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(holder.phone.getContext());
            builder.setTitle("Bạn chắc chắn chưa?");
            builder.setMessage("Dữ liệu không thể khôi phục sau khi xóa.");
            builder.setPositiveButton("Xóa", (dialogInterface, i) -> FirebaseDatabase.getInstance().getReference()
                    .child("scheduleBookings")
                    .child(Objects.requireNonNull(getRef(holder.getBindingAdapterPosition()).getKey()))
                    .removeValue());
            builder.setNegativeButton("Quay lại", (dialogInterface, i) -> {
            });
            builder.show();
        });

        holder.listScheduleBooking.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(view.getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_medical_examination_schedule_booking_details_dialog);
            Window window = dialog.getWindow();
            if (window == null) {
                return;
            }

            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams windowAttributes = window.getAttributes();
            windowAttributes.gravity = Gravity.CENTER;
            window.setAttributes(windowAttributes);

            TextView name, phone, content, date, time, describeSymptoms, doctorName, doctorPhone, address;
            Button ok;
            name = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_full_name_textview);
            phone = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_phone_textview);
            content = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_content_textview);
            date = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_date_textview);
            time = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_time_textview);
            describeSymptoms = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_describe_symptoms_textview);
            doctorName = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_doctor_textview);
            doctorPhone = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_doctor_phone_textview);
            address = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_address_textview);
            ok = dialog.findViewById(R.id.layout_medical_examination_schedule_booking_details_ok_button);

            name.setText(model.getFullName());
            phone.setText(model.getPhone());
            content.setText(model.getContent());
            date.setText(model.getDate());
            time.setText(model.getTime());
            describeSymptoms.setText(model.getDescribeSymptoms());
            doctorName.setText(holder.follower.getText().toString());
            doctorPhone.setText(model.getDoctorPhone());
            address.setText(model.getFullName());

            ok.setOnClickListener(view1 -> {
                dialog.dismiss();
            });


            dialog.show();
//            PrefDetail.setDataDetailPhone(holder.phone.getContext(), model.getPhone());
//            holder.phone.getContext().startActivity(new Intent(holder.phone.getContext(), CareAndMedicalExaminationScheduleBookingActivity.class));
        });
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_schedule_booking, parent, false);
        return new ListMedicalExaminationScheduleBookingAdapter.myViewHolder(view);
    }

    class myViewHolder extends RecyclerView.ViewHolder {
        ImageView logoCalendar;
        TextView fullName, phone, follower;
        ImageButton update, delete;
        Button listScheduleBooking;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            update = itemView.findViewById(R.id.list_schedule_booking_update_button);
            delete = itemView.findViewById(R.id.list_schedule_booking_delete_button);
            logoCalendar = itemView.findViewById(R.id.list_schedule_booking_calendar_image);
            fullName = itemView.findViewById(R.id.list_schedule_booking_full_name_textview);
            phone = itemView.findViewById(R.id.list_schedule_booking_phone_text_view);
            delete = itemView.findViewById(R.id.list_schedule_booking_delete_button);
            follower = itemView.findViewById(R.id.tvFollower);
            listScheduleBooking = itemView.findViewById(R.id.list_schedule_booking_details_button);
        }
    }
}
