package com.mobile.homecare.adapter;

import android.app.AlertDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.doctor.ListCareAndMedicalExaminationScheduleBookingDoctorActivity;
import com.mobile.homecare.activity.doctor.UpdateDoctorActivity;
import com.mobile.homecare.models.Doctor;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.Objects;

public class ListDoctorAdapter extends FirebaseRecyclerAdapter<Doctor, ListDoctorAdapter.myViewHolder> {

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ListDoctorAdapter(@NonNull FirebaseRecyclerOptions<Doctor> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull myViewHolder holder, int position, @NonNull Doctor model) {
        holder.fullName.setText(model.getFullName());
        holder.phone.setText(model.getPhone());
        DatabaseReference refMajor = FirebaseDatabase.getInstance().getReference();
        refMajor.child("majors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.child(model.getMajorUuid()).exists()) {
                    holder.major.setText(snapshot.child(model.getMajorUuid()).child("name").getValue(String.class));
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
        holder.update.setOnClickListener(view -> {
            PrefDetail.setDataDetailPhone(holder.phone.getContext(),model.getPhone());
            holder.phone.getContext().startActivity(new Intent(holder.phone.getContext(), UpdateDoctorActivity.class));
        });

        holder.delete.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(holder.phone.getContext());
            builder.setTitle("Bạn chắc chắn chưa?");
            builder.setMessage("Dữ liệu không thể khôi phục sau khi xóa.");
            builder.setPositiveButton("Xóa", (dialogInterface, i) -> FirebaseDatabase.getInstance().getReference()
                    .child("doctors")
                    .child(Objects.requireNonNull(getRef(holder.getAdapterPosition()).getKey()))
                    .removeValue());

            builder.setNegativeButton("Quay lại", (dialogInterface, i) -> {
            });
            builder.show();
        });

        holder.workSchedule.setOnClickListener(view -> {
            PrefDetail.setDataDetailPhone(holder.phone.getContext(),model.getPhone());
            holder.phone.getContext().startActivity(new Intent(holder.phone.getContext(), ListCareAndMedicalExaminationScheduleBookingDoctorActivity.class));
        });
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_doctor, parent, false);
        return new myViewHolder(view);
    }

    static class myViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView fullName, phone;
        TextView major;
        ImageButton update, delete;
        Button workSchedule;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);

            update = itemView.findViewById(R.id.list_schedule_booking_update_button);
            img = itemView.findViewById(R.id.imgAvatar);
            fullName = itemView.findViewById(R.id.list_schedule_booking_full_name_textview);
            phone = itemView.findViewById(R.id.list_schedule_booking_phone_text_view);
            major = itemView.findViewById(R.id.tvMajor);
            delete = itemView.findViewById(R.id.list_schedule_booking_delete_button);
            workSchedule = itemView.findViewById(R.id.list_doctor_work_schedule_button);
        }
    }
}
