package com.mobile.homecare.adapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.employee.UpdateEmployeeActivity;
import com.mobile.homecare.models.User;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.Objects;

public class ListEmployeeAdapter extends FirebaseRecyclerAdapter<User, ListEmployeeAdapter.myViewHolder> {


    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ListEmployeeAdapter(@NonNull FirebaseRecyclerOptions<User> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull myViewHolder holder, int position, @NonNull User model) {
        holder.fullName.setText(model.getName());
        holder.phone.setText(model.getPhone());
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PrefDetail.setDataDetailPhone(holder.phone.getContext(),model.getPhone());
                holder.phone.getContext().startActivity(new Intent(holder.phone.getContext(), UpdateEmployeeActivity.class));
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(holder.phone.getContext());
                builder.setTitle("Bạn chắc chắn chưa?");
                builder.setMessage("Dữ liệu không thể khôi phục sau khi xóa.");
                builder.setPositiveButton("Xóa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        FirebaseDatabase.getInstance().getReference()
                                .child("users")
                                .child(Objects.requireNonNull(getRef(holder.getAdapterPosition()).getKey()))
                                .removeValue();
                    }
                });

                builder.setNegativeButton("Quay lại", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.show();
            }
        });

    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ListEmployeeAdapter.myViewHolder(view);
    }

    class myViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView fullName, phone;
        ImageButton update, delete;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);

            update = itemView.findViewById(R.id.list_schedule_booking_update_button);
            img = itemView.findViewById(R.id.imgAvatar);
            fullName = itemView.findViewById(R.id.list_schedule_booking_full_name_textview);
            phone = itemView.findViewById(R.id.list_schedule_booking_phone_text_view);
            delete = itemView.findViewById(R.id.list_schedule_booking_delete_button);
        }
    }
}
