package com.mobile.homecare.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.mobile.homecare.fragments.ListCareScheduleBookingDoctorFragment;
import com.mobile.homecare.fragments.ListCareScheduleBookingFragment;
import com.mobile.homecare.fragments.ListMedicalExaminationScheduleBookingDoctorFragment;
import com.mobile.homecare.fragments.ListMedicalExaminationScheduleBookingFragment;

public class ListCareAndMedicalExaminationScheduleBookingDoctorAdapter extends FragmentStateAdapter {


    public ListCareAndMedicalExaminationScheduleBookingDoctorAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0) {
            return new ListMedicalExaminationScheduleBookingDoctorFragment();
        } else {
            return new ListCareScheduleBookingDoctorFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

}
