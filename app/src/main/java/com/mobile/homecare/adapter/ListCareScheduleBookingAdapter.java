package com.mobile.homecare.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.models.CareScheduleBooking;

import java.util.Objects;

public class ListCareScheduleBookingAdapter extends FirebaseRecyclerAdapter<CareScheduleBooking, ListCareScheduleBookingAdapter.myViewHolder> {

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ListCareScheduleBookingAdapter(@NonNull FirebaseRecyclerOptions<CareScheduleBooking> options) {
        super(options);
    }


    @Override
    protected void onBindViewHolder(@NonNull myViewHolder holder, int position, @NonNull CareScheduleBooking model) {
        holder.fullName.setText(model.getFullName());
        holder.phone.setText(model.getPhone());
        DatabaseReference refFollower = FirebaseDatabase.getInstance().getReference();
        refFollower.child("doctors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.child(model.getCareTakerPhone()).exists()) {
                    holder.follower.setText(snapshot.child(model.getCareTakerPhone()).child("fullName").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        holder.delete.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(holder.phone.getContext());
            builder.setTitle("Bạn chắc chắn chưa?");
            builder.setMessage("Dữ liệu không thể khôi phục sau khi xóa.");
            builder.setPositiveButton("Xóa", (dialogInterface, i) -> FirebaseDatabase.getInstance().getReference()
                    .child("scheduleBookings")
                    .child(Objects.requireNonNull(getRef(holder.getBindingAdapterPosition()).getKey()))
                    .removeValue());
            builder.setNegativeButton("Quay lại", (dialogInterface, i) -> {
            });
            builder.show();
        });

        holder.listScheduleBooking.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(view.getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_care_schedule_booking_details_dialog);
            Window window = dialog.getWindow();
            if (window == null) {
                return;
            }
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams windowAttributes = window.getAttributes();
            windowAttributes.gravity = Gravity.CENTER;
            window.setAttributes(windowAttributes);

            TextView name, phone, content, fromDate, toDate, careTime, followerName, followerPhone, address;
            Button ok;
            name = dialog.findViewById(R.id.layout_care_schedule_booking_details_full_name_textview);
            phone = dialog.findViewById(R.id.layout_care_schedule_booking_details_phone_textview);
            content = dialog.findViewById(R.id.layout_care_schedule_booking_details_content_textview);
            fromDate = dialog.findViewById(R.id.layout_care_schedule_booking_details_fromDate_textview);
            toDate = dialog.findViewById(R.id.layout_care_schedule_booking_details_toDate_textview);
            careTime = dialog.findViewById(R.id.layout_care_schedule_booking_details_care_time_textview);
            followerName = dialog.findViewById(R.id.layout_care_schedule_booking_details_follower_textview);
            followerPhone = dialog.findViewById(R.id.layout_care_schedule_booking_details_follower_phone_textview);
            address = dialog.findViewById(R.id.layout_care_schedule_booking_details_address_textview);
            ok = dialog.findViewById(R.id.layout_care_schedule_booking_details_ok_button);

            name.setText(model.getFullName());
            phone.setText(model.getPhone());
            content.setText(model.getContent());
            fromDate.setText(model.getFromDate());
            toDate.setText(model.getToDate());
            careTime.setText(model.getCareTime());
            followerName.setText(holder.follower.getText().toString());
            followerPhone.setText(model.getCareTakerPhone());
            address.setText(model.getAddress());

            ok.setOnClickListener(view1 -> {
                dialog.dismiss();
            });

            dialog.show();
//            PrefDetail.setDataDetailPhone(holder.phone.getContext(), model.getPhone());
//            holder.phone.getContext().startActivity(new Intent(holder.phone.getContext(), CareAndMedicalExaminationScheduleBookingActivity.class));
        });
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_schedule_booking, parent, false);
        return new ListCareScheduleBookingAdapter.myViewHolder(view);
    }

    class myViewHolder extends RecyclerView.ViewHolder {
        ImageView logoCalendar;
        TextView fullName, phone, follower;
        ImageButton update, delete;
        Button listScheduleBooking;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            update = itemView.findViewById(R.id.list_schedule_booking_update_button);
            delete = itemView.findViewById(R.id.list_schedule_booking_delete_button);
            logoCalendar = itemView.findViewById(R.id.list_schedule_booking_calendar_image);
            fullName = itemView.findViewById(R.id.list_schedule_booking_full_name_textview);
            phone = itemView.findViewById(R.id.list_schedule_booking_phone_text_view);
            delete = itemView.findViewById(R.id.list_schedule_booking_delete_button);
            follower = itemView.findViewById(R.id.tvFollower);
            listScheduleBooking = itemView.findViewById(R.id.list_schedule_booking_details_button);
        }
    }
}
