package com.mobile.homecare.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.mobile.homecare.fragments.CareScheduleBookingFragment;
import com.mobile.homecare.fragments.ListCareScheduleBookingFragment;
import com.mobile.homecare.fragments.ListMedicalExaminationScheduleBookingFragment;
import com.mobile.homecare.fragments.MedicalExaminationScheduleBookingFragment;

public class ListCareAndMedicalExaminationScheduleBookingAdapter extends FragmentStateAdapter {


    public ListCareAndMedicalExaminationScheduleBookingAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0) {
            return new ListMedicalExaminationScheduleBookingFragment();
        } else {
            return new ListCareScheduleBookingFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

}
