package com.mobile.homecare.activity.doctor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mobile.homecare.R;

import java.util.Objects;

public class UpdateWorkSchedule extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_work_schedule);
        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callBackListDoctorScreen(View view) {
        startActivity(new Intent(UpdateWorkSchedule.this, ListDoctorActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void chooseFromDate(View view) {
    }

    public void chooseToDate(View view) {
    }
}