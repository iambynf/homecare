package com.mobile.homecare.activity.admin;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.models.Status;
import com.mobile.homecare.models.User;
import com.mobile.homecare.preferences.PrefDetail;
import com.mobile.homecare.preferences.PrefLogin;

import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

public class VerifyOTPActivity extends AppCompatActivity {

    private static final String TAG = "VerifyOTP";
    TextView yourPhoneNumber;
    String codeBySystem;
    ProgressBar progressBar;
    Button verify;
    EditText otp1, otp2, otp3, otp4, otp5, otp6;
    User user;
    Status status = new Status();
    FirebaseDatabase rootNode;
    DatabaseReference reference;
    TreeMap<String, String> positionTree = new TreeMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        Objects.requireNonNull(getSupportActionBar()).hide();

        otp1 = findViewById(R.id.verify_otp_1);
        otp2 = findViewById(R.id.verify_otp_2);
        otp3 = findViewById(R.id.verify_otp_3);
        otp4 = findViewById(R.id.verify_otp_4);
        otp5 = findViewById(R.id.verify_otp_5);
        otp6 = findViewById(R.id.verify_otp_6);
        yourPhoneNumber = findViewById(R.id.tvYourPhoneNumber);
        verify = findViewById(R.id.verify_otp_button);
        progressBar = findViewById(R.id.progress_bar);

        setupOTPInputs();
        getPosition();

        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            String fullName, phone, password, gender, birthday, address, roleUuid;
            fullName = PrefDetail.getDataDetailFullName(VerifyOTPActivity.this);
            phone = PrefDetail.getDataDetailPhone(VerifyOTPActivity.this);
            password = PrefDetail.getDataDetailPassword(VerifyOTPActivity.this);
            gender = PrefDetail.getDataDetailGender(VerifyOTPActivity.this);
            birthday = PrefDetail.getDataDetailBirthday(VerifyOTPActivity.this);
            address = PrefDetail.getDataDetailAddress(VerifyOTPActivity.this);
            roleUuid = PrefDetail.getDataDetailPositionUuid(VerifyOTPActivity.this);
            Toast.makeText(VerifyOTPActivity.this, roleUuid, Toast.LENGTH_SHORT).show();
            user = new User(fullName, phone, password, address, birthday, gender, status.getActive(), roleUuid);
            sendVerificationCodeToUser(phone);
        }, 1000);
    }

    private void setupOTPInputs() {
        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    otp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    otp3.requestFocus();
                } else {
                    otp1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    otp4.requestFocus();
                } else {
                    otp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    otp5.requestFocus();
                } else {
                    otp3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        otp5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    otp6.requestFocus();
                } else {
                    otp4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        otp6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    verify.requestFocus();
                } else {
                    otp5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void sendVerificationCodeToUser(String phone) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        // [START start_phone_auth]
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(firebaseAuth)
                        .setPhoneNumber("+84" + phone)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                            @Override
                            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                                Log.e(TAG, "onVerificationCompleted: " + phoneAuthCredential.getSmsCode());
                            }

                            @Override
                            public void onVerificationFailed(@NonNull FirebaseException e) {
                                Log.e(TAG, "onVerificationFailed: " + e.getMessage());
                            }

                            @Override
                            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                Log.e(TAG, "onCodeSent: success");
                                codeBySystem = s;
                                Toast.makeText(VerifyOTPActivity.this, s, Toast.LENGTH_SHORT).show();
                                super.onCodeSent(s, forceResendingToken);
                            }
                        })          // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
        // [END start_phone_auth]
    }

    public void verifyOTP(View view) {
        progressBar.setVisibility(View.VISIBLE);
        verify.setVisibility(View.INVISIBLE);
        if (otp1.getText().toString().trim().isEmpty() ||
                otp2.getText().toString().trim().isEmpty() ||
                otp3.getText().toString().trim().isEmpty() ||
                otp4.getText().toString().trim().isEmpty() ||
                otp5.getText().toString().trim().isEmpty() ||
                otp6.getText().toString().trim().isEmpty()) {
            Toast.makeText(VerifyOTPActivity.this, "OTP is not Valid!", Toast.LENGTH_SHORT).show();
        } else if (codeBySystem != null) {
            String codeByUserInput = otp1.getText().toString().trim() +
                    otp2.getText().toString().trim() +
                    otp3.getText().toString().trim() +
                    otp4.getText().toString().trim() +
                    otp5.getText().toString().trim() +
                    otp6.getText().toString().trim();
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(codeBySystem, codeByUserInput);
            FirebaseAuth.getInstance()
                    .signInWithCredential(credential)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.VISIBLE);
                            verify.setVisibility(View.INVISIBLE);
                            rootNode = FirebaseDatabase.getInstance();
                            reference = rootNode.getReference("users");
                            reference.child(user.getPhone()).setValue(user);
                            final Handler handler = new Handler();
                            handler.postDelayed(() -> {
                                // Do something after 1s = 1000ms
                                PrefLogin.clearData(VerifyOTPActivity.this);
                                PrefDetail.clearDataDetail(VerifyOTPActivity.this);
                                Toast.makeText(VerifyOTPActivity.this, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(VerifyOTPActivity.this, SignInActivity.class));
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                            }, 1000);
                        } else {
                            progressBar.setVisibility(View.GONE);
                            verify.setVisibility(View.VISIBLE);
                            Toast.makeText(VerifyOTPActivity.this, "Sai mã OTP", Toast.LENGTH_SHORT).show();

                        }
                    });

        }
    }

    private void getPosition() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("roles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    positionTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                    PrefDetail.setDataDetailPositionUuid(VerifyOTPActivity.this, positionTree.get("Employee"));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}