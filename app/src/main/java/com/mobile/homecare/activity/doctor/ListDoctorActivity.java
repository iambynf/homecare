package com.mobile.homecare.activity.doctor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.homecare.R;
import com.mobile.homecare.adapter.ListDoctorAdapter;
import com.mobile.homecare.models.Doctor;

import java.util.Objects;

public class ListDoctorActivity extends AppCompatActivity {

    RecyclerView listDoctor;
    SearchView searchByName;
    ListDoctorAdapter listDoctorNursingAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_doctor);
        Objects.requireNonNull(getSupportActionBar()).hide();

        listDoctor = findViewById(R.id.fm_list_care_schedule_booking_recyclerview);
        searchByName = findViewById(R.id.searchByName);
        listDoctor.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<Doctor> options = new FirebaseRecyclerOptions.Builder<Doctor>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("doctors"), Doctor.class)
                .build();
        listDoctorNursingAdapter = new ListDoctorAdapter(options);
        listDoctor.setAdapter(listDoctorNursingAdapter);

        searchByName.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchByName(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchByName(newText);
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        listDoctorNursingAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        listDoctorNursingAdapter.stopListening();
    }

    public void callBackDoctorManagementScreen(View view) {
        startActivity(new Intent(ListDoctorActivity.this, DoctorManagementActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void searchByName(String str) {
        FirebaseRecyclerOptions<Doctor> options = new FirebaseRecyclerOptions.Builder<Doctor>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("doctors").orderByChild("fullName").startAt(str).endAt(str + "~"), Doctor.class)
                .build();

        listDoctorNursingAdapter = new ListDoctorAdapter(options);
        listDoctorNursingAdapter.startListening();
        listDoctor.setAdapter(listDoctorNursingAdapter);
    }
}