package com.mobile.homecare.activity.booking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.mobile.homecare.R;
import com.mobile.homecare.adapter.ListCareAndMedicalExaminationScheduleBookingAdapter;

import java.util.Objects;

public class ListCareAndMedicalExaminationScheduleBookingActivity extends AppCompatActivity {

    TabLayout tlListCareScheduleBooking;
    ViewPager2 vpListCareScheduleBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_care_and_medical_examination_schedule_booking);
        Objects.requireNonNull(getSupportActionBar()).hide();
        tlListCareScheduleBooking = findViewById(R.id.list_care_schedule_booking_tablayout);
        vpListCareScheduleBooking = findViewById(R.id.list_care_schedule_booking_viewpager2);

        FragmentManager fragmentManager = getSupportFragmentManager();
        ListCareAndMedicalExaminationScheduleBookingAdapter scheduleBookingAdapter = new ListCareAndMedicalExaminationScheduleBookingAdapter(fragmentManager, getLifecycle());
        vpListCareScheduleBooking.setAdapter(scheduleBookingAdapter);

        tlListCareScheduleBooking.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpListCareScheduleBooking.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        vpListCareScheduleBooking.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tlListCareScheduleBooking.selectTab(tlListCareScheduleBooking.getTabAt(position));
            }
        });
    }

    public void callBackScheduleBookingManagementScreen(View view) {
        startActivity(new Intent(ListCareAndMedicalExaminationScheduleBookingActivity.this, ScheduleBookingManagementActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}