package com.mobile.homecare.activity.patient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.doctor.ListDoctorActivity;
import com.mobile.homecare.models.Customer;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class UpdatePatientActivity extends AppCompatActivity {

    //Variables
    TextInputEditText phone, fullName, address, birthday, medicalHistoryDescription;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    AutoCompleteTextView autoGender, autoFollower;
    TreeMap<String, String> followerTree = new TreeMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_patient);
        Objects.requireNonNull(getSupportActionBar()).hide();
        autoGender = findViewById(R.id.autoGender);
        birthday = findViewById(R.id.etBirthday);
        fullName = findViewById(R.id.etFullName);
        address = findViewById(R.id.etAddress);
        phone = findViewById(R.id.etPhone);
        autoFollower = findViewById(R.id.autoFollower);
        medicalHistoryDescription = findViewById(R.id.etMedicalHistoryDescription);
        phone.setText(PrefDetail.getDataDetailPhone(this));
        String[] arrayGender = { "Nam", "Nữ" };
        ArrayList<String> arrayFollower = new ArrayList<>();
        ArrayAdapter<String> adapterGender = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayGender);
        ArrayAdapter<String> adapterFollower = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayFollower);
        getFollower(arrayFollower);
        getPatientDetails();
        autoFollower.setAdapter(adapterFollower);
        autoGender.setAdapter(adapterGender);
        onDateSetListener = (datePicker, year, month, dayOfMonth) -> {
            month = month + 1;
            String date = dayOfMonth + "/" + month + "/" +year;
            birthday.setText(date);
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callBackListPatientScreen(View view) {
        startActivity(new Intent(UpdatePatientActivity.this, ListPatientActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void chooseBirthday(View view) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(UpdatePatientActivity.this, onDateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void updateData(View view) {
        String phone = Objects.requireNonNull(this.phone.getText()).toString();
        String fullName, address, birthday, gender, follower, medicalHistoryDescription;
        fullName = Objects.requireNonNull(this.fullName.getText()).toString();
        address = Objects.requireNonNull(this.address.getText()).toString();
        birthday = Objects.requireNonNull(this.birthday.getText()).toString();
        gender = this.autoGender.getText().toString();
        follower = this.autoFollower.getText().toString();
        medicalHistoryDescription = Objects.requireNonNull(this.medicalHistoryDescription.getText()).toString();
        Customer customer = new Customer(fullName, gender, birthday, address, followerTree.get(follower), medicalHistoryDescription);
        Map<String, Object> customerValues = customer.toMap();
        DatabaseReference refUpdateData = FirebaseDatabase.getInstance().getReference("customers");
        refUpdateData.child(phone).updateChildren(customerValues).addOnSuccessListener(unused -> {
            Toast.makeText(UpdatePatientActivity.this, "Cập nhật thông tin thành công", Toast.LENGTH_SHORT).show();
            final Handler handler = new Handler();
            handler.postDelayed(() -> {
                // Do something after 1s = 1000ms
                PrefDetail.clearDataDetail(UpdatePatientActivity.this);
                startActivity(new Intent(UpdatePatientActivity.this, ListPatientActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }, 1000);
        }).addOnFailureListener(e -> Toast.makeText(UpdatePatientActivity.this, "Cập nhật thông tin thất bại, \nvui lòng kiểm tra lại các thông tin", Toast.LENGTH_SHORT).show());
    }

    public void getPatientDetails() {
        String phone = Objects.requireNonNull(this.phone.getText()).toString();
        DatabaseReference refPatientDetail = FirebaseDatabase.getInstance().getReference();
        refPatientDetail.child("customers").child(phone).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                fullName.setText(snapshot.child("fullName").getValue(String.class));
                address.setText(snapshot.child("address").getValue(String.class));
                birthday.setText(snapshot.child("birthday").getValue(String.class));
                autoGender.setText(snapshot.child("gender").getValue(String.class), false);
                String followerPhone = snapshot.child("followerPhone").getValue(String.class);
                assert followerPhone != null;
                refPatientDetail.child("doctors").child(followerPhone).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        autoFollower.setText(snapshot.child("fullName").getValue(String.class), false);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
                medicalHistoryDescription.setText(snapshot.child("medicalHistoryDescription").getValue(String.class));
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void getFollower(ArrayList<String> arrayFollower) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("doctors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    followerTree.put(dataSnapshot.child("fullName").getValue(String.class), dataSnapshot.child("phone").getValue(String.class));
                    arrayFollower.add(dataSnapshot.child("fullName").getValue(String.class));
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}