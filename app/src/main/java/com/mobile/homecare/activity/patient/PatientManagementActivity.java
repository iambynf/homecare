package com.mobile.homecare.activity.patient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mobile.homecare.R;
import com.mobile.homecare.activity.admin.MainActivity;
import com.mobile.homecare.activity.employee.EmployeeManagementActivity;
import com.mobile.homecare.activity.employee.ListEmployeeActivity;
import com.mobile.homecare.activity.employee.SignUpEmployeeActivity;

import java.util.Objects;

public class PatientManagementActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_management);
        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    public void callBackPatientManagementScreen(View view) {
        startActivity(new Intent(PatientManagementActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callSignUpPatientScreen(View view) {
        startActivity(new Intent(PatientManagementActivity.this, SignUpPatientActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void callListPatientScreen(View view) {
        startActivity(new Intent(PatientManagementActivity.this, ListPatientActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}