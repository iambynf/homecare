package com.mobile.homecare.activity.patient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.doctor.ListCareAndMedicalExaminationScheduleBookingDoctorActivity;
import com.mobile.homecare.activity.doctor.ListDoctorActivity;
import com.mobile.homecare.adapter.ListCareAndMedicalExaminationScheduleBookingDoctorAdapter;
import com.mobile.homecare.adapter.ListCareAndMedicalExaminationScheduleBookingPatientAdapter;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.Objects;

public class ListCareAndMedicalExaminationScheduleBookingPatientActivity extends AppCompatActivity {

    TabLayout tlListCareAndMedicalExaminationScheduleBookingDoctor;
    ViewPager2 vpListCareAndMedicalExaminationScheduleBookingDoctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_care_and_medical_examination_schedule_booking_patient);
        Objects.requireNonNull(getSupportActionBar()).hide();

        tlListCareAndMedicalExaminationScheduleBookingDoctor = findViewById(R.id.list_care_and_medical_examination_schedule_booking_patient_tabLayout);
        vpListCareAndMedicalExaminationScheduleBookingDoctor = findViewById(R.id.list_care_and_medical_examination_schedule_booking_patient_viewpager2);

        FragmentManager fragmentManager = getSupportFragmentManager();
        ListCareAndMedicalExaminationScheduleBookingPatientAdapter scheduleBookingPatientAdapter = new ListCareAndMedicalExaminationScheduleBookingPatientAdapter(fragmentManager, getLifecycle());
        vpListCareAndMedicalExaminationScheduleBookingDoctor.setAdapter(scheduleBookingPatientAdapter);

        tlListCareAndMedicalExaminationScheduleBookingDoctor.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpListCareAndMedicalExaminationScheduleBookingDoctor.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        vpListCareAndMedicalExaminationScheduleBookingDoctor.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tlListCareAndMedicalExaminationScheduleBookingDoctor.selectTab(tlListCareAndMedicalExaminationScheduleBookingDoctor.getTabAt(position));
            }
        });
    }

    public void callBackListPatientScreen(View view) {
        PrefDetail.clearDataDetail(ListCareAndMedicalExaminationScheduleBookingPatientActivity.this);
        startActivity(new Intent(ListCareAndMedicalExaminationScheduleBookingPatientActivity.this, ListPatientActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}