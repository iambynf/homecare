package com.mobile.homecare.activity.employee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.homecare.R;
import com.mobile.homecare.adapter.ListEmployeeAdapter;
import com.mobile.homecare.models.User;

import java.util.Objects;

public class ListEmployeeActivity extends AppCompatActivity {

    RecyclerView listEmployee;
    SearchView searchByName;
    ListEmployeeAdapter listEmployeeAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_employee);
        Objects.requireNonNull(getSupportActionBar()).hide();

        listEmployee = findViewById(R.id.rvListEmployee);
        searchByName = findViewById(R.id.searchByName);
        listEmployee.setLayoutManager(new LinearLayoutManager(this));
        FirebaseRecyclerOptions<User> options = new FirebaseRecyclerOptions.Builder<User>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("users").orderByChild("roleUuid").equalTo("55838e09-df8a-4016-b2ec-b681a6ac07f8"), User.class)
                .build();
        listEmployeeAdapter = new ListEmployeeAdapter(options);
        listEmployee.setAdapter(listEmployeeAdapter);

        searchByName.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchByName(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchByName(newText);
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        listEmployeeAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        listEmployeeAdapter.stopListening();
    }

    public void callBackEmployeeManagementScreen(View view) {
        startActivity(new Intent(ListEmployeeActivity.this, EmployeeManagementActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void searchByName(String str) {
        FirebaseRecyclerOptions<User> options = new FirebaseRecyclerOptions.Builder<User>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("users").orderByChild("name").startAt(str).endAt(str + "~").orderByChild("roleUuid"), User.class)
                .build();

        listEmployeeAdapter = new ListEmployeeAdapter(options);
        listEmployeeAdapter.startListening();
        listEmployee.setAdapter(listEmployeeAdapter);
    }
}