package com.mobile.homecare.activity.employee;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mobile.homecare.R;
import com.mobile.homecare.activity.admin.MainActivity;
import com.mobile.homecare.activity.doctor.DoctorManagementActivity;
import com.mobile.homecare.activity.doctor.ListDoctorActivity;
import com.mobile.homecare.activity.doctor.SignUpDoctorActivity;

import java.util.Objects;

public class EmployeeManagementActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_management);
        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    public void callBackEmployeeManagementScreen(View view) {
        startActivity(new Intent(EmployeeManagementActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callSignUpEmployeeScreen(View view) {
        startActivity(new Intent(EmployeeManagementActivity.this, SignUpEmployeeActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void callListEmployeeScreen(View view) {
        startActivity(new Intent(EmployeeManagementActivity.this, ListEmployeeActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}