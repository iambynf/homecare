package com.mobile.homecare.activity.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.mobile.homecare.R;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.Objects;

public class SignUpActivity extends AppCompatActivity {
    //Variables
    ImageView backBtn, imageProgress;
    Button next, login;
    TextView titleText;
    TextInputEditText fullName, phone, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Objects.requireNonNull(getSupportActionBar()).hide();

        //Hooks
        backBtn = findViewById(R.id.signup_back_button);
        next = findViewById(R.id.signup_next_button);
        login = findViewById(R.id.signup_login_button);
        titleText = findViewById(R.id.signup_title_text);
        imageProgress = findViewById(R.id.signup_image_progress);
        fullName = findViewById(R.id.etFullName);
        phone = findViewById(R.id.etPhone);
        password = findViewById(R.id.etPassword);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callNextSignUpScreen(View view) {

        if(!validateName() || !validatePhone() || !validatePassword()) {
            return;
        }
        Intent intent = new Intent(getApplicationContext(), SignUp2ndActivity.class);
        String fullName = Objects.requireNonNull(this.fullName.getText()).toString();
        String phone = Objects.requireNonNull(this.phone.getText()).toString();
        String password = Objects.requireNonNull(this.password.getText()).toString();
        PrefDetail.setDataDetail(this, true);
        PrefDetail.setDataDetailFullName(this, fullName);
        PrefDetail.setDataDetailPhone(this, phone);
        PrefDetail.setDataDetailPassword(this, password);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

    }

    public void callBackSignInScreen(View view) {
        PrefDetail.clearDataDetail(SignUpActivity.this);
        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    //Validations

    public boolean validateName() {
        String val = Objects.requireNonNull(fullName.getText()).toString();
        if(val.isEmpty()) {
            fullName.setError("Field cannot be empty");
            return false;
        } else {
            fullName.setError(null);
            return true;
        }
    }

    public boolean validatePhone() {
        String val = Objects.requireNonNull(phone.getText()).toString();
        String pattern = "(84|0[3|5|7|8|9])+([0-9]{8})\\b";
        if(val.isEmpty()) {
            phone.setError("Field cannot be empty");
            return false;
        } else if(!val.matches(pattern)){
            phone.setError("Invalid phone number");
            return false;
        } else {
            phone.setError(null);
            return true;
        }
    }

    public boolean validatePassword() {
        String val = Objects.requireNonNull(password.getText()).toString();
        String pattern = "^" +
                "(?=\\S+$)" + //no white spaces
                ".{4,}"; // anything, at least eight places though
        if(val.isEmpty()) {
            password.setError("Field cannot be empty");
            return false;
        } else if(!val.matches(pattern)){
            password.setError("Password is too weak");
            return false;
        } else {
            password.setError(null);
            return true;
        }
    }
}