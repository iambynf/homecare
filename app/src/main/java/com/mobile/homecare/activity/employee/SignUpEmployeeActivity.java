package com.mobile.homecare.activity.employee;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.doctor.SignUpDoctorActivity;
import com.mobile.homecare.models.Status;
import com.mobile.homecare.models.User;

import java.util.Calendar;
import java.util.Objects;
import java.util.TreeMap;

public class SignUpEmployeeActivity extends AppCompatActivity {
    TextInputEditText fullName, phone, address, birthday, password;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    Status status = new Status();
    AutoCompleteTextView autoGender;
    TreeMap<String, String> positionTree = new TreeMap<>();
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_employee);
        Objects.requireNonNull(getSupportActionBar()).hide();
        autoGender = findViewById(R.id.autoGender);
        birthday = findViewById(R.id.etBirthday);
        fullName = findViewById(R.id.etFullName);
        phone = findViewById(R.id.etPhone);
        address = findViewById(R.id.etAddress);
        password = findViewById(R.id.etPassword);
        String[] arrayGender = { "Nam", "Nữ" };
        ArrayAdapter<String> adapterGender = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, arrayGender);
        autoGender.setAdapter(adapterGender);
        getPosition();
        onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = dayOfMonth + "/" + month + "/" +year;
                birthday.setText(date);
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callBackEmployeeManagementScreen(View view) {
        startActivity(new Intent(SignUpEmployeeActivity.this, EmployeeManagementActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void chooseBirthday(View view) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                SignUpEmployeeActivity.this,
                onDateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void getPosition() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("roles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (Objects.equals(dataSnapshot.child("name").getValue(String.class), "Employee")) {
                        positionTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void signUpEmployee(View view) {
        String fullName, phone, gender, address, birthday, password;
        fullName = Objects.requireNonNull(this.fullName.getText()).toString();
        phone = Objects.requireNonNull(this.phone.getText()).toString();
        gender = autoGender.getText().toString();
        address = Objects.requireNonNull(this.address.getText()).toString();
        birthday = Objects.requireNonNull(this.birthday.getText()).toString();
        password = Objects.requireNonNull(this.password.getText()).toString();
        User user =  new User(fullName, phone, password, address, birthday, gender, status.getActive(), positionTree.get("Employee"));
        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("users");
        reference.child(user.getPhone()).setValue(user);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 1s = 1000ms
                Toast.makeText( SignUpEmployeeActivity.this, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(SignUpEmployeeActivity.this, EmployeeManagementActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        }, 1000);
    }
}