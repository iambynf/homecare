package com.mobile.homecare.activity.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;

import com.mobile.homecare.R;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.Objects;

public class SignUp2ndActivity extends AppCompatActivity {

    //Variables
    RadioButton male, female;
    DatePicker birthday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up2nd);
        Objects.requireNonNull(getSupportActionBar()).hide();

        //Hooks
        male = findViewById(R.id.sign_up_2nd_male_radio);
        female = findViewById(R.id.sign_up_2nd_female_radio);
        birthday = findViewById(R.id.sign_up_2nd_birthday_date_picker);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callNextSignUpScreen(View view) {
        Intent intent = new Intent(getApplicationContext(), SignUp3rdActivity.class);
        String gender = male.getText().toString();
        int day = this.birthday.getDayOfMonth();
        int month = this.birthday.getMonth() + 1;
        int year = this.birthday.getYear();
        String birthday = day +
                "-" + month +
                "-" + year;
        if (female.isChecked()) {
            gender = female.getText().toString();
        }
        PrefDetail.setDataDetailGender(this, gender);
        PrefDetail.setDataDetailBirthday(this, birthday);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void callBackSignUpScreen(View view) {
        Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}