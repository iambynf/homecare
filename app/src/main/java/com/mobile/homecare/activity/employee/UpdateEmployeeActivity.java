package com.mobile.homecare.activity.employee;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.doctor.ListDoctorActivity;
import com.mobile.homecare.activity.doctor.UpdateDoctorActivity;
import com.mobile.homecare.models.User;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.Calendar;
import java.util.Map;
import java.util.Objects;

public class UpdateEmployeeActivity extends AppCompatActivity {

    //Variables
    TextInputEditText phone, fullName, address, birthday;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    AutoCompleteTextView autoGender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_employee);
        Objects.requireNonNull(getSupportActionBar()).hide();
        autoGender = findViewById(R.id.autoGender);
        birthday = findViewById(R.id.etBirthday);
        fullName = findViewById(R.id.etFullName);
        address = findViewById(R.id.etAddress);
        phone = findViewById(R.id.etPhone);
        phone.setText(PrefDetail.getDataDetailPhone(this));
        String[] arrayGender = { "Nam", "Nữ" };
        getInformation();
        ArrayAdapter<String> adapterGender = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, arrayGender);
        autoGender.setAdapter(adapterGender);
        onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = dayOfMonth + "/" + month + "/" +year;
                birthday.setText(date);
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callBackListEmployeeScreen(View view) {
        startActivity(new Intent(UpdateEmployeeActivity.this, ListEmployeeActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void chooseBirthday(View view) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateEmployeeActivity.this, onDateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void updateData(View view) {
        String phone = this.phone.getText().toString();
        String fullName, address, birthday, gender;
        fullName = this.fullName.getText().toString();
        address = this.address.getText().toString();
        birthday = this.birthday.getText().toString();
        gender = this.autoGender.getText().toString();
        User user = new User(fullName, address, birthday, gender);
        Map<String, Object> userValues = user.toMap();
        DatabaseReference refUpdateData = FirebaseDatabase.getInstance().getReference("users");
        refUpdateData.child(phone).updateChildren(userValues).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(UpdateEmployeeActivity.this, "Cập nhật thông tin thành công", Toast.LENGTH_SHORT).show();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 1s = 1000ms
                        PrefDetail.clearDataDetail(UpdateEmployeeActivity.this);
                        startActivity(new Intent(UpdateEmployeeActivity.this, ListDoctorActivity.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                    }
                }, 1000);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(UpdateEmployeeActivity.this, "Cập nhật thông tin thất bại, \nvui lòng kiểm tra lại các thông tin", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getInformation() {
        String phone = this.phone.getText().toString();
        DatabaseReference refDoctorDetail = FirebaseDatabase.getInstance().getReference();
        refDoctorDetail.child("users").child(phone).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                fullName.setText(snapshot.child("name").getValue(String.class));
                address.setText(snapshot.child("address").getValue(String.class));
                birthday.setText(snapshot.child("birthday").getValue(String.class));
                autoGender.setText(snapshot.child("gender").getValue(String.class), false);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}