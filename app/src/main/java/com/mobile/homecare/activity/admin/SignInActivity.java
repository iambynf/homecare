package com.mobile.homecare.activity.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.preferences.PrefDetail;
import com.mobile.homecare.preferences.PrefLogin;

import java.util.Objects;


public class SignInActivity extends AppCompatActivity {

    //Variables
    EditText phone, password;
    CheckBox saveInformation;
    TextView titleText;
    public static String account = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Objects.requireNonNull(getSupportActionBar()).hide();

        phone = findViewById(R.id.sign_in_phone_edit_text);
        password = findViewById(R.id.sign_in_password_edit_text);
        titleText = findViewById(R.id.login_title_text);
        saveInformation = findViewById(R.id.sign_in_save_information_combo_box);
    }

    public void loginAccount(View view) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String phone = SignInActivity.this.phone.getText().toString().trim();
                String password = SignInActivity.this.password.getText().toString().trim();
                if (snapshot.child(phone).exists() && Objects.equals(snapshot.child(phone).child("password").getValue(String.class), password)) {
                    if (saveInformation.isChecked()) {
                        if (Objects.equals(snapshot.child(phone).child("roleUuid").getValue(String.class), "849b9df4-01b3-4c04-aa22-ad20abce45a7")) {
                            //If click save, your login information saved
                            PrefLogin.setDataLogin(SignInActivity.this, true);
                            PrefLogin.setDataRole(SignInActivity.this, "Admin");
                            PrefLogin.setDataPhone(SignInActivity.this, phone);
                            account = SignInActivity.this.phone.getText().toString();
                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                        } else {
                            PrefLogin.setDataLogin(SignInActivity.this, true);
                            PrefLogin.setDataRole(SignInActivity.this, "Employee");
                            PrefLogin.setDataPhone(SignInActivity.this, phone);
                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                        }
                    } else {
                        if (Objects.equals(snapshot.child(phone).child("roleUuid").getValue(String.class), "849b9df4-01b3-4c04-aa22-ad20abce45a7")) {
                            PrefLogin.setDataLogin(SignInActivity.this, false);
                            PrefLogin.setDataRole(SignInActivity.this, "Admin");
                            PrefLogin.setDataPhone(SignInActivity.this, phone);
                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                        } else {
                            PrefLogin.setDataLogin(SignInActivity.this, false);
                            PrefLogin.setDataRole(SignInActivity.this, "Employee");
                            PrefLogin.setDataPhone(SignInActivity.this, phone);
                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                        }
                    }
                } else {
                    Toast.makeText(SignInActivity.this, "Sai tài khoản hoặc mật khẩu", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (PrefLogin.getDataLogin(this)) {
            PrefDetail.clearDataDetail(this);
            if (PrefLogin.getDataRole(this).equals("Admin")) {
                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                finish();
            } else if (PrefLogin.getDataRole(this).equals("Employee")) {
                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                finish();
            }
        } else {
            PrefDetail.clearDataDetail(this);
            PrefLogin.clearData(this);
        }
    }

    public void callSignUpScreen(View view) {
        Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}