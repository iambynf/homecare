package com.mobile.homecare.activity.doctor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.mobile.homecare.R;
import com.mobile.homecare.activity.admin.MainActivity;

import java.util.Objects;

public class DoctorManagementActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_management);
        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    public void callBackDoctorManagementScreen(View view) {
        startActivity(new Intent(DoctorManagementActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callSignUpDoctorNursingScreen(View view) {
        startActivity(new Intent(DoctorManagementActivity.this, SignUpDoctorActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void callListDoctorNursingScreen(View view) {
        startActivity(new Intent(DoctorManagementActivity.this, ListDoctorActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}