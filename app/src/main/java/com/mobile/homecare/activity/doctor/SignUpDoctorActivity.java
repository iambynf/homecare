package com.mobile.homecare.activity.doctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.models.Doctor;
import com.mobile.homecare.models.Status;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.TreeMap;

public class SignUpDoctorActivity extends AppCompatActivity {

    //Variables
    TextInputEditText fullName, phone, address, birthday, password;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    AutoCompleteTextView autoGender, autoMajor, autoExperience, autoJobType, autoPosition;
    Status status = new Status();
    FirebaseDatabase rootNode;
    DatabaseReference reference;
    TreeMap<String, String> majorTree = new TreeMap<>();
    TreeMap<String, String> positionTree = new TreeMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_doctor);
        Objects.requireNonNull(getSupportActionBar()).hide();
        autoGender = findViewById(R.id.autoGender);
        autoMajor = findViewById(R.id.autoMajor);
        autoExperience = findViewById(R.id.autoExperience);
        autoJobType = findViewById(R.id.autoJobType);
        autoPosition = findViewById(R.id.autoPosition);
        birthday = findViewById(R.id.etBirthday);
        fullName = findViewById(R.id.etFullName);
        phone = findViewById(R.id.etPhone);
        address = findViewById(R.id.etAddress);
        password = findViewById(R.id.etPassword);
        String[] arrayGender = { "Nam", "Nữ" };
        String[] arrayJobType = {"FullTime", "PartTime"};
        String[] arrayExperience = {"Trên 5 năm", "Từ 3 đến 5 năm", "Từ 1 đến 3 năm", "Dưới 1 năm", "Chưa có kinh nghiệm"};
        ArrayList<String> arrayPosition = new ArrayList<>();
        ArrayList<String> arrayMajor = new ArrayList<>();
        ArrayAdapter<String> adapterGender = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, arrayGender);
        ArrayAdapter<String> adapterMajor = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, arrayMajor);
        ArrayAdapter<String> adapterExperience = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayExperience);
        ArrayAdapter<String> adapterJobType = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayJobType);
        ArrayAdapter<String> adapterPosition = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayPosition);
        getMajor(arrayMajor);
        getPosition(arrayPosition);
        autoGender.setAdapter(adapterGender);
        autoMajor.setAdapter(adapterMajor);
        autoJobType.setAdapter(adapterJobType);
        autoPosition.setAdapter(adapterPosition);
        autoExperience.setAdapter(adapterExperience);
        onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = dayOfMonth + "/" + month + "/" +year;
                birthday.setText(date);
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void callBackDoctorManagementScreen(View view) {
        startActivity(new Intent(SignUpDoctorActivity.this, DoctorManagementActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void SignUp(View view) {
        String fullName, phone, address, birthday, password, gender, experience, jobType, positionUuid, majorUuid;
        fullName = Objects.requireNonNull(this.fullName.getText()).toString();
        phone = Objects.requireNonNull(this.phone.getText()).toString();
        address = Objects.requireNonNull(this.address.getText()).toString();
        birthday = Objects.requireNonNull(this.birthday.getText()).toString();
        password = Objects.requireNonNull(this.password.getText()).toString();
        gender = this.autoGender.getText().toString();
        experience = this.autoExperience.getText().toString();
        jobType = this.autoJobType.getText().toString();
        majorUuid = majorTree.get(autoMajor.getText().toString());
        positionUuid = positionTree.get(autoPosition.getText().toString());
        Doctor doctor = new Doctor(fullName, gender, birthday, address, phone, password, status.getActive(), jobType, majorUuid, experience, positionUuid);
        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("doctors");
        reference.child(doctor.getPhone()).setValue(doctor);
        Toast.makeText(SignUpDoctorActivity.this, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 1s = 1000ms
                startActivity(new Intent(SignUpDoctorActivity.this, DoctorManagementActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        }, 1000);
    }

    private void getMajor(ArrayList<String> arrayMajor) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("majors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    majorTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                    arrayMajor.add(dataSnapshot.child("name").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void getPosition(ArrayList<String> arrayPosition) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("roles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (Objects.equals(dataSnapshot.child("name").getValue(String.class), "Doctor") || Objects.equals(dataSnapshot.child("name").getValue(String.class), "Nursing")) {
                        positionTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                        arrayPosition.add(dataSnapshot.child("name").getValue(String.class));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void chooseBirthday(View view) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(SignUpDoctorActivity.this, onDateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }
}