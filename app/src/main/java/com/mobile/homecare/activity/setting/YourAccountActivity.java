package com.mobile.homecare.activity.setting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.activity.doctor.ListDoctorActivity;
import com.mobile.homecare.activity.employee.UpdateEmployeeActivity;
import com.mobile.homecare.models.User;
import com.mobile.homecare.preferences.PrefDetail;
import com.mobile.homecare.preferences.PrefLogin;

import java.util.Calendar;
import java.util.Map;
import java.util.Objects;

public class YourAccountActivity extends AppCompatActivity {

    TextInputEditText fullName, phone, address, birthday;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    AutoCompleteTextView gender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_account);
        Objects.requireNonNull(getSupportActionBar()).hide();

        fullName = findViewById(R.id.change_password_full_name_edit_text);
        phone = findViewById(R.id.change_password_phone_edit_text);
        gender = findViewById(R.id.change_password_gender_auto);
        address = findViewById(R.id.change_password_address_edit_text);
        birthday = findViewById(R.id.change_password_birthday_edit_text);
        phone.setText(PrefLogin.getDataPhone(this));
        getInformation(Objects.requireNonNull(phone.getText()).toString());
        String[] arrayGender = { "Nam", "Nữ" };
        ArrayAdapter<String> adapterGender = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayGender);
        gender.setAdapter(adapterGender);
        onDateSetListener = (datePicker, year, month, dayOfMonth) -> {
            month = month + 1;
            String date = dayOfMonth + "/" + month + "/" +year;
            birthday.setText(date);
        };

    }

    public void chooseBirthday(View view) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(YourAccountActivity.this, onDateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void getInformation(String phone) {
        DatabaseReference refDoctorDetail = FirebaseDatabase.getInstance().getReference();
        refDoctorDetail.child("users").child(phone).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                fullName.setText(snapshot.child("name").getValue(String.class));
                address.setText(snapshot.child("address").getValue(String.class));
                birthday.setText(snapshot.child("birthday").getValue(String.class));
                gender.setText(snapshot.child("gender").getValue(String.class), false);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    public void callBackSettingScreen(View view) {
        startActivity(new Intent(YourAccountActivity.this, SettingActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void updateData(View view) {
        String phone = this.phone.getText().toString();
        String fullName, address, birthday, gender;
        fullName = this.fullName.getText().toString();
        address = this.address.getText().toString();
        birthday = this.birthday.getText().toString();
        gender = this.gender.getText().toString();
        User user = new User(fullName, address, birthday, gender);
        Map<String, Object> userValues = user.toMap();
        DatabaseReference refUpdateData = FirebaseDatabase.getInstance().getReference("users");
        refUpdateData.child(phone).updateChildren(userValues).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(YourAccountActivity.this, "Cập nhật thông tin thành công", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(YourAccountActivity.this, "Cập nhật thông tin thất bại, \nvui lòng kiểm tra lại các thông tin", Toast.LENGTH_SHORT).show();
            }
        });
    }
}