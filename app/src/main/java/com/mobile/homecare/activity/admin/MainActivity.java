package com.mobile.homecare.activity.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.activity.booking.ScheduleBookingManagementActivity;
import com.mobile.homecare.activity.doctor.DoctorManagementActivity;
import com.mobile.homecare.activity.employee.EmployeeManagementActivity;
import com.mobile.homecare.activity.patient.PatientManagementActivity;
import com.mobile.homecare.activity.setting.SettingActivity;
import com.mobile.homecare.preferences.PrefLogin;
import com.mobile.homecare.R;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    Button menu, home;
    TextView tvFullName, tvPhone;
    String phone;
    String role;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).hide();
        phone = PrefLogin.getDataPhone(this);
        role = PrefLogin.getDataRole(this);
        menu = findViewById(R.id.btnMenu);
        home = findViewById(R.id.btnHome);
        tvFullName = findViewById(R.id.list_schedule_booking_full_name_textview);
        tvPhone = findViewById(R.id.list_schedule_booking_phone_text_view);

        //Get name and phone of current user
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.child(phone).exists()) {
                    tvFullName.setText(snapshot.child(phone).child("name").getValue(String.class));
                    tvPhone.setText(snapshot.child(phone).child("phone").getValue(String.class));
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    /**
     * Show the bottom sheet dialog, which has choices for additional displays.
     * */
    public void showDialog(View view) {
        BottomSheetDialog dialog = new BottomSheetDialog(MainActivity.this, R.style.BottomSheetDialogTheme);
        dialog.setContentView(R.layout.bottom_sheet_layout);
        LinearLayout doctorNursingManagement, employeeManagement, patientManagement, bookingManagement, settings, logout;
        ImageView lineStraightEmployee;
        doctorNursingManagement = dialog.findViewById(R.id.lnlDoctorNursingManagement);
        patientManagement = dialog.findViewById(R.id.lnlPatientManagement);
        bookingManagement = dialog.findViewById(R.id.lnlBookingManagement);
        employeeManagement = dialog.findViewById(R.id.lnlEmployeeManagement);
        settings = dialog.findViewById(R.id.lnlSettings);
        logout = dialog.findViewById(R.id.lnlLogout);
        lineStraightEmployee = dialog.findViewById(R.id.imgLineStraightEmployee);

        //Delegation of administrative and employee rights
        if(role.equals("Employee")) {
            assert employeeManagement != null;
            employeeManagement.setVisibility(View.GONE);
            assert lineStraightEmployee != null;
            lineStraightEmployee.setVisibility(View.GONE);
        }
        assert doctorNursingManagement != null;
        doctorNursingManagement.setOnClickListener(view1 -> {
            Toast.makeText(MainActivity.this, "Quản lý bác sĩ, điều dưỡng", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            startActivity(new Intent(MainActivity.this, DoctorManagementActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

        });
        assert patientManagement != null;
        patientManagement.setOnClickListener(view2 -> {
            Toast.makeText(MainActivity.this, "Quản lý bệnh nhân", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            startActivity(new Intent(MainActivity.this, PatientManagementActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        });
        assert bookingManagement != null;
        bookingManagement.setOnClickListener(view3 -> {
            Toast.makeText(MainActivity.this, "Quản lý đặt lịch", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            startActivity(new Intent(MainActivity.this, ScheduleBookingManagementActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        });
        assert employeeManagement != null;
        employeeManagement.setOnClickListener(view4 -> {
            Toast.makeText(MainActivity.this, "Quản lý nhân viên", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            startActivity(new Intent(MainActivity.this, EmployeeManagementActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        });
        assert settings != null;
        settings.setOnClickListener(view4 -> {
            Toast.makeText(MainActivity.this, "Cài đặt", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            startActivity(new Intent(MainActivity.this, SettingActivity.class));
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        });
        assert logout != null;
        logout.setOnClickListener(view4 -> {
            dialog.dismiss();
            startActivity(new Intent(MainActivity.this, SignInActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            PrefLogin.clearData(MainActivity.this);
            finish();
        });
        dialog.show();
    }
}