package com.mobile.homecare.activity.booking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mobile.homecare.R;
import com.mobile.homecare.activity.admin.MainActivity;

import java.util.Objects;

public class ScheduleBookingManagementActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_booking_management);
        Objects.requireNonNull(getSupportActionBar()).hide();
    }

    public void callBackMainScreen(View view) {
        startActivity(new Intent(ScheduleBookingManagementActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callScheduleBookingScreen(View view) {
        startActivity(new Intent(ScheduleBookingManagementActivity.this, CareAndMedicalExaminationScheduleBookingActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void callListScheduleBookingScreen(View view) {
        startActivity(new Intent(ScheduleBookingManagementActivity.this, ListCareAndMedicalExaminationScheduleBookingActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }
}