package com.mobile.homecare.activity.patient;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.models.Customer;
import com.mobile.homecare.models.Status;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.TreeMap;

public class SignUpPatientActivity extends AppCompatActivity {
    TextInputEditText fullName, phone, address, birthday, password, medicalHistoryDescription;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    AutoCompleteTextView autoGender, autoFollower;
    TreeMap<String, String> positionTree = new TreeMap<>();
    TreeMap<String, String> followerTree = new TreeMap<>();
    Status status = new Status();
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_patient);
        Objects.requireNonNull(getSupportActionBar()).hide();
        autoGender = findViewById(R.id.autoGender);
        autoFollower = findViewById(R.id.autoFollower);
        medicalHistoryDescription = findViewById(R.id.etMedicalHistoryDescription);
        birthday = findViewById(R.id.etBirthday);
        fullName = findViewById(R.id.etFullName);
        phone = findViewById(R.id.etPhone);
        address = findViewById(R.id.etAddress);
        password = findViewById(R.id.etPassword);
        String[] arrayGender = {"Nam", "Nữ"};
        ArrayList<String> arrayFollower = new ArrayList<>();
        ArrayAdapter<String> adapterGender = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayGender);
        ArrayAdapter<String> adapterFollower = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayFollower);
        getFollower(arrayFollower);
        autoFollower.setAdapter(adapterFollower);
        autoGender.setAdapter(adapterGender);
        getPosition();
        onDateSetListener = (datePicker, year, month, dayOfMonth) -> {
            month = month + 1;
            String date = dayOfMonth + "/" + month + "/" + year;
            birthday.setText(date);
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void callBackPatientManagementScreen(View view) {
        startActivity(new Intent(SignUpPatientActivity.this, PatientManagementActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void SignUp(View view) {
        String fullName, phone, gender, address, birthday, password, follower, medicalHistoryDescription;
        fullName = Objects.requireNonNull(this.fullName.getText()).toString();
        phone = Objects.requireNonNull(this.phone.getText()).toString();
        gender = autoGender.getText().toString();
        address = Objects.requireNonNull(this.address.getText()).toString();
        birthday = Objects.requireNonNull(this.birthday.getText()).toString();
        password = Objects.requireNonNull(this.password.getText()).toString();
        follower = this.autoFollower.getText().toString();
        medicalHistoryDescription = Objects.requireNonNull(this.medicalHistoryDescription.getText()).toString();
        Customer customer = new Customer(fullName, gender, birthday, address, phone, password, status.getActive(), followerTree.get(follower), medicalHistoryDescription, positionTree.get("Patient"));
        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("customers");
        reference.child(customer.phone).setValue(customer);
        Toast.makeText(SignUpPatientActivity.this, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            // Do something after 1s = 1000ms
            startActivity(new Intent(SignUpPatientActivity.this, PatientManagementActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }, 1000);
    }

    public void chooseBirthday(View view) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(SignUpPatientActivity.this,
                onDateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void getPosition() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("roles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (Objects.equals(dataSnapshot.child("name").getValue(String.class), "Patient")) {
                        positionTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getFollower(ArrayList<String> arrayFollower) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("doctors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    followerTree.put(dataSnapshot.child("fullName").getValue(String.class), dataSnapshot.child("phone").getValue(String.class));
                    arrayFollower.add(dataSnapshot.child("fullName").getValue(String.class));
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}