package com.mobile.homecare.activity.booking;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.mobile.homecare.R;

public class MedicalExaminationScheduleBookingDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_examination_schedule_booking_details);
    }
}