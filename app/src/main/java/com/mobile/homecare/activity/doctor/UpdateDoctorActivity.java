package com.mobile.homecare.activity.doctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.models.Doctor;
import com.mobile.homecare.preferences.PrefDetail;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class UpdateDoctorActivity extends AppCompatActivity {

    TextInputEditText fullName, address, birthday;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    AutoCompleteTextView autoGender, autoMajor, autoExperience, autoJobType, autoPosition;
    TreeMap<String, String> majorTree = new TreeMap<>();
    TreeMap<String, String> positionTree = new TreeMap<>();
    String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_doctor);
        Objects.requireNonNull(getSupportActionBar()).hide();
        autoGender = findViewById(R.id.autoGender);
        autoMajor = findViewById(R.id.autoMajor);
        autoExperience = findViewById(R.id.autoExperience);
        autoJobType = findViewById(R.id.autoJobType);
        autoPosition = findViewById(R.id.autoPosition);
        birthday = findViewById(R.id.etBirthday);
        fullName = findViewById(R.id.etFullName);
        address = findViewById(R.id.etAddress);
        phone = PrefDetail.getDataDetailPhone(this);
        String[] arrayGender = { "Nam", "Nữ" };
        String[] arrayJobType = {"FullTime", "PartTime"};
        String[] arrayExperience = {"Trên 5 năm", "Từ 3 đến 5 năm", "Từ 1 đến 3 năm", "Dưới 1 năm", "Chưa có kinh nghiệm"};
        ArrayList<String> arrayPosition = new ArrayList<>();
        ArrayList<String> arrayMajor = new ArrayList<>();
        ArrayAdapter<String> adapterGender = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayGender);
        ArrayAdapter<String> adapterMajor = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayMajor);
        ArrayAdapter<String> adapterExperience = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayExperience);
        ArrayAdapter<String> adapterJobType = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayJobType);
        ArrayAdapter<String> adapterPosition = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, arrayPosition);
        getDoctorDetails();
        getMajor(arrayMajor);
        getPosition(arrayPosition);
        autoGender.setAdapter(adapterGender);
        autoMajor.setAdapter(adapterMajor);
        autoJobType.setAdapter(adapterJobType);
        autoPosition.setAdapter(adapterPosition);
        autoExperience.setAdapter(adapterExperience);
        onDateSetListener = (datePicker, year, month, dayOfMonth) -> {
            month = month + 1;
            String date = dayOfMonth + "/" + month + "/" +year;
            birthday.setText(date);
        };

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callBackListDoctorScreen(View view) {
        startActivity(new Intent(UpdateDoctorActivity.this, ListDoctorActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void chooseBirthday(View view) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateDoctorActivity.this, onDateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void updateData(View view) {
        String fullName, gender, address, birthday, majorUuid, experience, jobType, positionUuid;
        fullName = Objects.requireNonNull(this.fullName.getText()).toString();
        gender = this.autoGender.getText().toString();
        address = Objects.requireNonNull(this.address.getText()).toString();
        birthday = Objects.requireNonNull(this.birthday.getText()).toString();
        experience = this.autoExperience.getText().toString();
        majorUuid = majorTree.get(this.autoMajor.getText().toString());
        jobType = this.autoJobType.getText().toString();
        positionUuid = positionTree.get(this.autoPosition.getText().toString());
        Doctor doctor = new Doctor(fullName, gender, birthday, address, jobType, majorUuid, experience, positionUuid);
        Map<String, Object> doctorValues = doctor.toMap();
        DatabaseReference refUpdateData = FirebaseDatabase.getInstance().getReference("doctors");
        refUpdateData.child(phone).updateChildren(doctorValues).addOnSuccessListener(unused -> {
            Toast.makeText(UpdateDoctorActivity.this, "Cập nhật thông tin thành công", Toast.LENGTH_SHORT).show();
            final Handler handler = new Handler();
            handler.postDelayed(() -> {
                // Do something after 1s = 1000ms
                PrefDetail.clearDataDetail(UpdateDoctorActivity.this);
                startActivity(new Intent(UpdateDoctorActivity.this, ListDoctorActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }, 1000);
        }).addOnFailureListener(e -> Toast.makeText(UpdateDoctorActivity.this, "Cập nhật thông tin thất bại, \nvui lòng kiểm tra lại các thông tin", Toast.LENGTH_SHORT).show());
    }

    /**
     * Lấy thông tin của bác sĩ từ số điện thoại
     * */
    public void getDoctorDetails() {
        DatabaseReference refDoctorDetail = FirebaseDatabase.getInstance().getReference();
        refDoctorDetail.child("doctors").child(phone).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                fullName.setText(snapshot.child("fullName").getValue(String.class));
                address.setText(snapshot.child("address").getValue(String.class));
                birthday.setText(snapshot.child("birthday").getValue(String.class));
                autoGender.setText(snapshot.child("gender").getValue(String.class), false);
                autoExperience.setText(snapshot.child("experience").getValue(String.class), false);
                autoJobType.setText(snapshot.child("jobType").getValue(String.class), false);
                String majorUuid = snapshot.child("majorUuid").getValue(String.class);
                String roleUuid = snapshot.child("roleUuid").getValue(String.class);
                assert majorUuid != null;
                refDoctorDetail.child("majors").child(majorUuid).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        autoMajor.setText(snapshot.child("name").getValue(String.class), false);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                    }
                });
                assert roleUuid != null;
                refDoctorDetail.child("roles").child(roleUuid).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        autoPosition.setText(snapshot.child("name").getValue(String.class), false);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                    }
                });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getMajor(ArrayList<String> arrayMajor) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("majors").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    majorTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                    arrayMajor.add(dataSnapshot.child("name").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void getPosition(ArrayList<String> arrayPosition) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("roles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (Objects.equals(dataSnapshot.child("name").getValue(String.class), "Doctor") || Objects.equals(dataSnapshot.child("name").getValue(String.class), "Nursing")) {
                        positionTree.put(dataSnapshot.child("name").getValue(String.class), dataSnapshot.child("uuid").getValue(String.class));
                        arrayPosition.add(dataSnapshot.child("name").getValue(String.class));
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}