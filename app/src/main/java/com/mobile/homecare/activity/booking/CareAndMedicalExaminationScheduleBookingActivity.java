package com.mobile.homecare.activity.booking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.mobile.homecare.R;
import com.mobile.homecare.adapter.CareAndMedicalExaminationScheduleBookingAdapter;

import java.util.Objects;

public class CareAndMedicalExaminationScheduleBookingActivity extends AppCompatActivity {

    TabLayout tlScheduleBooking;
    ViewPager2 vpScheduleBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_care_and_medical_examination_schedule_booking);
        Objects.requireNonNull(getSupportActionBar()).hide();

        tlScheduleBooking = findViewById(R.id.list_care_schedule_booking_tablayout);
        vpScheduleBooking = findViewById(R.id.list_care_schedule_booking_viewpager2);

        FragmentManager fragmentManager = getSupportFragmentManager();
        CareAndMedicalExaminationScheduleBookingAdapter scheduleBookingAdapter = new CareAndMedicalExaminationScheduleBookingAdapter(fragmentManager, getLifecycle());
        vpScheduleBooking.setAdapter(scheduleBookingAdapter);

        tlScheduleBooking.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpScheduleBooking.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        vpScheduleBooking.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tlScheduleBooking.selectTab(tlScheduleBooking.getTabAt(position));
            }
        });
    }

    public void callScheduleBookingManagementScreen(View view) {
        startActivity(new Intent(CareAndMedicalExaminationScheduleBookingActivity.this, ScheduleBookingManagementActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}