package com.mobile.homecare.activity.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.mobile.homecare.R;
import com.mobile.homecare.preferences.PrefDetail;

import java.util.Objects;

public class SignUp3rdActivity extends AppCompatActivity {

    //Variables
    TextInputEditText address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up3rd);
        Objects.requireNonNull(getSupportActionBar()).hide();

        //Hooks
        address = findViewById(R.id.signup_3nd_address_edit_text);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void callNextSignUpScreen(View view) {
        Intent intent = new Intent(getApplicationContext(), VerifyOTPActivity.class);
        String address = Objects.requireNonNull(this.address.getText()).toString();
        PrefDetail.setDataDetailAddress(this, address);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public void callBackSignUpScreen(View view) {
        Intent intent = new Intent(getApplicationContext(), SignUp2ndActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}