package com.mobile.homecare.activity.patient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.homecare.R;
import com.mobile.homecare.adapter.ListPatientAdapter;
import com.mobile.homecare.models.Customer;

import java.util.Objects;

public class ListPatientActivity extends AppCompatActivity {

    RecyclerView listPatient;
    SearchView searchByName;
    ListPatientAdapter listPatientAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_patient);
        Objects.requireNonNull(getSupportActionBar()).hide();

        listPatient = findViewById(R.id.rvListPatient);
        searchByName = findViewById(R.id.searchByName);
        listPatient.setLayoutManager(new LinearLayoutManager(this));
        FirebaseRecyclerOptions<Customer> options = new FirebaseRecyclerOptions.Builder<Customer>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("customers"), Customer.class)
                .build();
        listPatientAdapter = new ListPatientAdapter(options);
        listPatient.setAdapter(listPatientAdapter);

        searchByName.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchByName(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchByName(newText);
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        listPatientAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        listPatientAdapter.stopListening();
    }

    public void callBackPatientManagementScreen(View view) {
        startActivity(new Intent(ListPatientActivity.this, PatientManagementActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void searchByName(String str) {
        FirebaseRecyclerOptions<Customer> options = new FirebaseRecyclerOptions.Builder<Customer>()
                .setQuery(FirebaseDatabase.getInstance().getReference().child("customers").orderByChild("fullName").startAt(str).endAt(str + "~"), Customer.class)
                .build();

        listPatientAdapter = new ListPatientAdapter(options);
        listPatientAdapter.startListening();
        listPatient.setAdapter(listPatientAdapter);
    }
}