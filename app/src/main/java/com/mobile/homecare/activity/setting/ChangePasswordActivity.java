package com.mobile.homecare.activity.setting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.homecare.R;
import com.mobile.homecare.preferences.PrefLogin;

import java.util.HashMap;
import java.util.Objects;

public class ChangePasswordActivity extends AppCompatActivity {

    TextInputEditText oldPassword, newPassword, retypeNewPassword;
    String phone, oldPasswordFromDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Objects.requireNonNull(getSupportActionBar()).hide();
        oldPassword = findViewById(R.id.change_password_old_password_edit_text);
        newPassword = findViewById(R.id.change_password_new_password_edit_text);
        retypeNewPassword = findViewById(R.id.change_password_retype_new_password_edit_text);
        phone = PrefLogin.getDataPhone(this);
        getOldPassword(phone);

    }

    public void callBackSettingScreen(View view) {
        startActivity(new Intent(ChangePasswordActivity.this, SettingActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void changePassword(View view) {
        if(oldPassword.getText().toString().equals(oldPasswordFromDatabase)) {
            if (newPassword.getText().toString().equals(retypeNewPassword.getText().toString())) {
                String newPassword = this.newPassword.getText().toString();
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("password", newPassword);
                DatabaseReference refChangePassword = FirebaseDatabase.getInstance().getReference("users");
                refChangePassword.child(phone).updateChildren(hashMap).addOnSuccessListener(o -> {
                    Toast.makeText(ChangePasswordActivity.this, "Thay đổi mật khẩu thành công", Toast.LENGTH_SHORT).show();
                }).addOnFailureListener(e -> {
                    Toast.makeText(ChangePasswordActivity.this, "Thay đổi mật khẩu thất bại!, vui lòng thử lại", Toast.LENGTH_SHORT).show();
                });
            } else {
                Toast.makeText(ChangePasswordActivity.this, "Mật khẩu mới không trùng khớp, vui lòng thử lại", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(ChangePasswordActivity.this, "Mật khẩu cũ không đúng, vui lòng thử lại.", Toast.LENGTH_SHORT).show();
        }
    }

    public void getOldPassword(String phone) {
        DatabaseReference refOldPassword = FirebaseDatabase.getInstance().getReference();
        refOldPassword.child("users").child(phone).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                oldPasswordFromDatabase = snapshot.child("password").getValue(String.class);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}