package com.mobile.homecare.models;

public class JobType {
    public String PartTime;
    public String FullTime;

    public JobType()
    {
        this.PartTime = "PartTime";
        this.FullTime = "FullTime";
    }

}
