package com.mobile.homecare.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class User {
    private String name;
    private String phone;
    private String password;
    private String address;
    private String birthday;
    private String gender;
    private String status;
    private String roleUuid;

    public User() {

    }

    public User(String name, String address, String birthday, String gender) {
        this.name = name;
        this.address = address;
        this.birthday = birthday;
        this.gender = gender;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("address", address);
        result.put("birthday", birthday);
        result.put("gender", gender);
        return result;
    }

    public User(String name, String phone, String password, String address, String birthday, String gender, String status, String roleUuid) {
        this.name = name;
        this.phone = phone;
        this.password = password;
        this.address = address;
        this.birthday = birthday;
        this.gender = gender;
        this.status = status;
        this.roleUuid = roleUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRoleUuid() {
        return roleUuid;
    }

    public void setRoleUuid(String roleUuid) {
        this.roleUuid = roleUuid;
    }
}
