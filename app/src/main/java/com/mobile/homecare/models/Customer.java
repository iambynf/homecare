package com.mobile.homecare.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Customer {
    public String fullName;
    public String gender;
    public String birthday;
    public String address;
    public String phone;
    public String password;
    public String status;
    public String followerPhone;
    public String medicalHistoryDescription;
    public String roleUuid;

    public Customer() {

    }

    public Customer(String fullName, String address, String phone, String followerPhone, String roleUuid) {
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
        this.followerPhone = followerPhone;
        this.roleUuid = roleUuid;
    }

    public Customer(String fullName, String gender, String birthday, String address, String phone, String password, String status, String followerPhone, String medicalHistoryDescription, String roleUuid) {
        this.fullName = fullName;
        this.gender = gender;
        this.birthday = birthday;
        this.address = address;
        this.phone = phone;
        this.password = password;
        this.status = status;
        this.followerPhone = followerPhone;
        this.medicalHistoryDescription = medicalHistoryDescription;
        this.roleUuid = roleUuid;
    }

    public Customer(String fullName, String gender, String birthday, String address, String followerPhone, String medicalHistoryDescription) {
        this.fullName = fullName;
        this.gender = gender;
        this.birthday = birthday;
        this.address = address;
        this.followerPhone = followerPhone;
        this.medicalHistoryDescription = medicalHistoryDescription;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("fullName", fullName);
        result.put("gender", gender);
        result.put("birthday", birthday);
        result.put("address", address);
        result.put("followerPhone", followerPhone);
        result.put("medicalHistoryDescription", medicalHistoryDescription);
        return result;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFollowerPhone() {
        return followerPhone;
    }

    public void setFollowerPhone(String followerPhone) {
        this.followerPhone = followerPhone;
    }

    public String getMedicalHistoryDescription() {
        return medicalHistoryDescription;
    }

    public void setMedicalHistoryDescription(String medicalHistoryDescription) {
        this.medicalHistoryDescription = medicalHistoryDescription;
    }

    public String getRoleUuid() {
        return roleUuid;
    }

    public void setRoleUuid(String roleUuid) {
        this.roleUuid = roleUuid;
    }
}
