package com.mobile.homecare.models;

public class CareScheduleBooking {
    private String uuid;
    private String fullName;
    private String phone;
    private String address;
    private String content;
    private String fromDate;
    private String toDate;
    private String careTime;
    private String careTakerPhone;
    private String content_phone;
    private String content_careTakerPhone;
    private String createAt;

    public CareScheduleBooking() {
    }

    public CareScheduleBooking(String uuid, String fullName, String phone, String address, String content, String fromDate, String toDate, String careTime, String careTakerPhone, String content_phone, String content_careTakerPhone, String createAt) {
        this.uuid = uuid;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
        this.content = content;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.careTime = careTime;
        this.careTakerPhone = careTakerPhone;
        this.content_phone = content_phone;
        this.content_careTakerPhone = content_careTakerPhone;
        this.createAt = createAt;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCareTime() {
        return careTime;
    }

    public void setCareTime(String careTime) {
        this.careTime = careTime;
    }

    public String getCareTakerPhone() {
        return careTakerPhone;
    }

    public void setCareTakerPhone(String careTakerPhone) {
        this.careTakerPhone = careTakerPhone;
    }

    public String getContent_phone() {
        return content_phone;
    }

    public void setContent_phone(String content_phone) {
        this.content_phone = content_phone;
    }

    public String getContent_careTakerPhone() {
        return content_careTakerPhone;
    }

    public void setContent_careTakerPhone(String content_careTakerPhone) {
        this.content_careTakerPhone = content_careTakerPhone;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }
}


