package com.mobile.homecare.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Doctor {
    public String fullName;
    public String gender;
    public String birthday;
    public String address;
    public String phone;
    public String password;
    public String status;
    public String jobType;
    public String majorUuid;
    public String experience;
    public String roleUuid;

    public Doctor() {
    }

    public Doctor(String fullName, String gender, String birthday, String address, String jobType, String majorUuid, String experience, String roleUuid) {
        this.fullName = fullName;
        this.gender = gender;
        this.birthday = birthday;
        this.address = address;
        this.jobType = jobType;
        this.majorUuid = majorUuid;
        this.experience = experience;
        this.roleUuid = roleUuid;
    }

    public Doctor(String fullName, String gender, String birthday, String address, String phone, String password, String status, String jobType, String majorUuid, String experience, String roleUuid) {
        this.fullName = fullName;
        this.gender = gender;
        this.birthday = birthday;
        this.address = address;
        this.phone = phone;
        this.password = password;
        this.status = status;
        this.jobType = jobType;
        this.majorUuid = majorUuid;
        this.experience = experience;
        this.roleUuid = roleUuid;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("fullName", fullName);
        result.put("gender", gender);
        result.put("birthday", birthday);
        result.put("address", address);
        result.put("jobType", jobType);
        result.put("majorUuid", majorUuid);
        result.put("experience", experience);
        result.put("roleUuid", roleUuid);
        return result;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getMajorUuid() {
        return majorUuid;
    }

    public void setMajorUuid(String majorUuid) {
        this.majorUuid = majorUuid;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getRoleUuid() {
        return roleUuid;
    }

    public void setRoleUuid(String roleUuid) {
        this.roleUuid = roleUuid;
    }
}
