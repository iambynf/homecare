package com.mobile.homecare.models;

public class Role {
    private int uuid;
    private String name;
    private String status;

    public Role() {
    }

    public Role(int uuid, String name, String status) {
        this.uuid = uuid;
        this.name = name;
        this.status = status;
    }

    public int getUuid() {
        return uuid;
    }

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
