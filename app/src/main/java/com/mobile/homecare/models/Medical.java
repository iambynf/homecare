package com.mobile.homecare.models;

public class Medical {
    private int uuid;
    private String name;

    public Medical() {
    }

    public Medical(int uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public int getUuid() {
        return uuid;
    }

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
