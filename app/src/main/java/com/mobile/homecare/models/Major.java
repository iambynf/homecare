package com.mobile.homecare.models;

import java.sql.Date;

public class Major {
    public int uuid;
    public String name;

    public Major() {
    }

    public Major(int uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public int getUuid() {
        return uuid;
    }

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
