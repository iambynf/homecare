package com.mobile.homecare.models;

public class MedicalExaminationScheduleBooking {
    private String uuid;
    private String fullName;
    private String phone;
    private String address;
    private String content;
    private String date;
    private String time;
    private String describeSymptoms;
    private String majorUuid;
    private String doctorPhone;
    private String content_phone;
    private String content_doctorPhone;
    private String createAt;

    public MedicalExaminationScheduleBooking() {
    }

    public MedicalExaminationScheduleBooking(String uuid, String fullName, String phone, String address, String content, String date, String time, String describeSymptoms, String majorUuid, String doctorPhone, String content_phone, String content_doctorPhone, String createAt) {
        this.uuid = uuid;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
        this.content = content;
        this.date = date;
        this.time = time;
        this.describeSymptoms = describeSymptoms;
        this.majorUuid = majorUuid;
        this.doctorPhone = doctorPhone;
        this.content_phone = content_phone;
        this.content_doctorPhone = content_doctorPhone;
        this.createAt = createAt;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescribeSymptoms() {
        return describeSymptoms;
    }

    public void setDescribeSymptoms(String describeSymptoms) {
        this.describeSymptoms = describeSymptoms;
    }

    public String getMajorUuid() {
        return majorUuid;
    }

    public void setMajorUuid(String majorUuid) {
        this.majorUuid = majorUuid;
    }

    public String getDoctorPhone() {
        return doctorPhone;
    }

    public void setDoctorPhone(String doctorPhone) {
        this.doctorPhone = doctorPhone;
    }

    public String getContent_phone() {
        return content_phone;
    }

    public void setContent_phone(String content_phone) {
        this.content_phone = content_phone;
    }

    public String getContent_doctorPhone() {
        return content_doctorPhone;
    }

    public void setContent_doctorPhone(String content_doctorPhone) {
        this.content_doctorPhone = content_doctorPhone;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }
}
