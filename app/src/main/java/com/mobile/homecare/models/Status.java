package com.mobile.homecare.models;

public class Status {
    private String Active;
    private String Disable;

    public Status() {
        this.Active = "Active";
        this.Disable = "Disable";
    }

    public String getActive() {
        return Active;
    }

    public String getDisable() {
        return Disable;
    }
}
